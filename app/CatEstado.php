<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatEstado extends Model
{
    //Use table own
    protected $table = 'catestados';

    protected $fillable = [
        'nombreEstado',
    ];
}
