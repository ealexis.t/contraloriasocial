<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatMunicipio extends Model
{
    //Use table own
    protected $table = 'catmunicipios';

    protected $fillable = [
        'idEstado',
        'nombreMunicipio',
    ];

    public static function municipios($id){

        return CatMunicipio::where('idEstado', '=', $id)->get();

    }
}
