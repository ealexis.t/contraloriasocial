<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Abogado extends Model
{
    //Use table own
    protected $table = 'abogado';
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'idUsers',
        'idTurnador',
    ];
}
