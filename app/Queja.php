<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Queja extends Model
{
    //Use table own
    protected $table = 'queja';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'calle',
        'numInt',
        'numExt',
        'colonia',
        'cp',
        'municipio',
        'fechaHechos',
        'horaAproximada',
        'nombreFuncionario',
        'apellidoPatFuncionario',
        'apellidoMatFuncionario',
        'cargoFuncionario',
        'areaAdscripcion',
        'dependenciaGobierno',
        'narrativa',
        'descripcionPruebas',
        'tipoPruebas',
        'visto',
        'estadoQueja',
        'comentarioRechazo',
        'idTipoQueja',
        'idQuejoso',
        'idEnlace',
        'idAbogado',
        'idPrograma',
    ];

}
