<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatInstituciones extends Model
{
    //Use table own
    protected $table = 'catinstituciones';

    protected $fillable = [
        'nombreInstitucion',
    ];
}
