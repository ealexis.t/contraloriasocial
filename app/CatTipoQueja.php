<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatTipoQueja extends Model
{
    //Use table own
    protected $table = 'cattipoqueja';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombreQueja',
        'descripcionQueja',
    ];
}
