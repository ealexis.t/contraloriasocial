<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quejoso extends Model
{
    //Use table own
    protected $table = 'quejoso';

    protected $fillable = [
        'nombre',
        'apPaterno',
        'apMaterno',
        'calle',
        'numInt',
        'numExt',
        'colonia',
        'cp',
        'municipio',
        'telefono',
        'email',
        'idEstado',
    ];
}
