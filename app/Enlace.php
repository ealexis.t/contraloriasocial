<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enlace extends Model
{
    //Use table own
    protected $table = 'enlace';
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'idEstado',
        'idUsers',
    ];
}
