<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoricoQueja extends Model
{
    //Use table own
    protected $table = 'historicoQuejas';

    protected $fillable = [
        'idQueja',
        'estadoQueja',
        'idEnlace',
        'idAbogado',
        'idTurnador',
    ];

}
