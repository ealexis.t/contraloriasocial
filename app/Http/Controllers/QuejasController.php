<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\CatMunicipio;
use App\CatPrograma;
use App\Abogado;
use App\Enlace;
use App\CatEstado;
use App\CatTipoQueja;
use App\CatInstituciones;
use App\Quejoso;
use App\Queja;
use DB;

class QuejasController extends Controller
{

 	protected $comboEstados;
 	protected $comboMunicipios;
 	protected $comboProgramas;
 	protected $comboTiposQuejas;
 	protected $comboInstituciones;

	public function __construct()
	{
	    $this->middleware('auth');

	     // Grab the logged in user if any and set it to this property.
	    // All extending classes should then have access to it.
	    $this->estados    	   = CatEstado::all();
	    $this->municipios 	   = CatMunicipio::all();
	    $this->programas   	   = CatPrograma::all();
	    $this->tiposQuejas     = CatTipoQueja::all();
	    $this->instituciones   = CatInstituciones::all();

	    $this->comboEstados       = [];
	    $this->comboMunicipios    = [];
	    $this->comboProgramas     = [];
	    $this->comboTiposQuejas   = [];
	    $this->comboInstituciones = [];

	    foreach ($this->instituciones as $institucion) {
	        $this->comboInstituciones[$institucion->id] = $institucion->nombreInstitucion;
	    }

	    foreach ($this->tiposQuejas as $tipoQueja) {
	        $this->comboTiposQuejas[$tipoQueja->id] = $tipoQueja->nombreQueja;
	    }

	    foreach ($this->programas as $programa) {
	        $this->comboProgramas[$programa->id] = $programa->nombrePrograma;
	    }

	    foreach ($this->municipios as $municipio) {
	        $this->comboMunicipios[$municipio->id] = $municipio->nombreMunicipio;
	    }

	    foreach ($this->estados as $estado) {
	        $this->comboEstados[$estado->id] = $estado->nombreEstado;
	    }

	    // Share this property with all the views in your application.
	    view()->share('comboEstados', $this->comboEstados);
	    view()->share('comboMunicipios', $this->comboMunicipios);
	    view()->share('comboProgramas', $this->comboProgramas);
	    view()->share('comboTiposQuejas', $this->comboTiposQuejas);
	    view()->share('comboInstituciones', $this->comboInstituciones);
	}


    public function index()
	{

		if(Auth::user()->categoria == 'Enlace'){

			$idEnlace = DB::table('enlace')->where('idUsers', Auth::user()->id)->value('id');

			$quejas = DB::table('queja')
							->where('idEnlace', $idEnlace)
							->where('estadoQueja', 'Pendiente')
							->orderBy('updated_at', 'desc')
							->paginate(20);

			$quejosos = DB::table('quejoso')->get();

			$estadosCat = DB::table('catestados')->get();

			$municipiosCat = DB::table('catmunicipios')->get();

			$tipoQuejasCat = DB::table('cattipoqueja')->get();

			$institucionescat = DB::table('catinstituciones')->get();

			$programascat = DB::table('catprograma')->get();



			return view('enlace.quejas.nuevas')
						->with('municipiosCat', $municipiosCat)
						->with('estadosCat', $estadosCat)
						->with('quejas', $quejas)
						->with('quejosos', $quejosos)
						->with('catestados', $estadosCat)
						->with('catmunicipios', $municipiosCat)
						->with('cattipoquejas', $tipoQuejasCat)
						->with('catinstituciones', $institucionescat)
						->with('catprogramas', $programascat)
						->with('estados', $this->comboEstados)
            			->with('programas', $this->comboProgramas)
            			->with('municipios', $this->comboMunicipios)
            			->with('tiposQuejas', $this->comboTiposQuejas)
            			->with('instituciones', $this->comboInstituciones);

		}
		elseif(Auth::user()->categoria == 'SuperAdministrador' || Auth::user()->categoria == 'Turnador'){

			$quejas = DB::table('queja')
							->where('idInstitucion', Auth::user()->idInstitucion)
							->where('estadoQueja', 'Pendiente')
							->orderBy('updated_at', 'desc')
							->paginate(20);

			$abogados = DB::table('users')
							->where('categoria', 'Abogado')
							->get();

			$idAbogados = DB::table('abogado')
							->get();

			$quejosos = DB::table('quejoso')->get();

			$estadosCat = DB::table('catestados')->get();

			$municipiosCat = DB::table('catmunicipios')->get();

			$tipoQuejasCat = DB::table('cattipoqueja')->get();

			$institucionescat = DB::table('catinstituciones')->get();

			$programascat = DB::table('catprograma')->get();

			return view('turnador.quejas.nuevas')
						->with('quejas', $quejas)
						->with('abogados', $abogados)
						->with('idAbogados', $idAbogados)

						->with('municipiosCat', $municipiosCat)
						->with('estadosCat', $estadosCat)
						->with('quejosos', $quejosos)
						->with('catestados', $estadosCat)
						->with('catmunicipios', $municipiosCat)
						->with('cattipoquejas', $tipoQuejasCat)
						->with('catinstituciones', $institucionescat)
						->with('catprogramas', $programascat)
						->with('estados', $this->comboEstados)
            			->with('programas', $this->comboProgramas)
            			->with('municipios', $this->comboMunicipios)
            			->with('tiposQuejas', $this->comboTiposQuejas)
            			->with('instituciones', $this->comboInstituciones);
		}
	}


	public function abiertas()
	{
		if(Auth::user()->categoria == 'Enlace'){

			$quejas = DB::table('queja')
							//->where('idEnlace', Auth::user()->id)
							->where('idEnlace', '1')
							->where('estadoQueja', 'Aceptada')
							->orderBy('updated_at', 'desc')
							->paginate(20);

			return view('enlace.quejas.abiertas')
						->with('quejas', $quejas)
						->with('estados', $this->comboEstados)
            			->with('programas', $this->comboProgramas)
            			->with('municipios', $this->comboMunicipios)
            			->with('tiposQuejas', $this->comboTiposQuejas)
            			->with('instituciones', $this->comboInstituciones);

		}
		elseif(Auth::user()->categoria == 'SuperAdministrador' || Auth::user()->categoria == 'Turnador'){

			$quejas = DB::table('queja')
							->where('idInstitucion', Auth::user()->idInstitucion)
							->where('estadoQueja', 'Aceptada')
							->orderBy('updated_at', 'desc')
							->paginate(20);

			$abogados = DB::table('users')
							->where('categoria', 'Abogado')
							->get();

			$idAbogados = DB::table('abogado')
							->get();

			return view('turnador.quejas.abiertas')
						->with('quejas', $quejas)
						->with('abogados', $abogados)
						->with('idAbogados', $idAbogados);
		}
	}

	public function aceptadas()
	{
		if(Auth::user()->categoria == 'Enlace'){

			$quejas = DB::table('queja')
							//->where('idEnlace', Auth::user()->id)
							->where('idEnlace', '1')
							->where('estadoQueja', 'Aceptada')
							->orderBy('updated_at', 'desc')
							->paginate(20);

			$quejosos = DB::table('quejoso')->get();

			$estadosCat = DB::table('catestados')->get();

			$municipiosCat = DB::table('catmunicipios')->get();

			$tipoQuejasCat = DB::table('cattipoqueja')->get();

			$institucionescat = DB::table('catinstituciones')->get();

			$programascat = DB::table('catprograma')->get();

			return view('enlace.quejas.aceptadas')
						->with('municipiosCat', $municipiosCat)
						->with('estadosCat', $estadosCat)
						->with('quejas', $quejas)
						->with('quejosos', $quejosos)
						->with('catestados', $estadosCat)
						->with('catmunicipios', $municipiosCat)
						->with('cattipoquejas', $tipoQuejasCat)
						->with('catinstituciones', $institucionescat)
						->with('catprogramas', $programascat)
						->with('estados', $this->comboEstados)
            			->with('programas', $this->comboProgramas)
            			->with('municipios', $this->comboMunicipios)
            			->with('tiposQuejas', $this->comboTiposQuejas)
            			->with('instituciones', $this->comboInstituciones);


		}
		elseif(Auth::user()->categoria == 'SuperAdministrador' || Auth::user()->categoria == 'Turnador'){

			$quejas = DB::table('queja')
							->where('idInstitucion', Auth::user()->idInstitucion)
							->where('estadoQueja', 'Aceptada')
							->orderBy('updated_at', 'desc')
							->paginate(20);

			$quejosos = DB::table('quejoso')->get();

			$estadosCat = DB::table('catestados')->get();

			$municipiosCat = DB::table('catmunicipios')->get();

			$tipoQuejasCat = DB::table('cattipoqueja')->get();

			$institucionescat = DB::table('catinstituciones')->get();

			$programascat = DB::table('catprograma')->get();

			$idAbogados = DB::table('abogado')
							->get();

			return view('turnador.quejas.aceptadas')
						->with('municipiosCat', $municipiosCat)
						->with('estadosCat', $estadosCat)
						->with('quejas', $quejas)
						->with('quejosos', $quejosos)
						->with('catestados', $estadosCat)
						->with('catmunicipios', $municipiosCat)
						->with('cattipoquejas', $tipoQuejasCat)
						->with('catinstituciones', $institucionescat)
						->with('catprogramas', $programascat)
						->with('estados', $this->comboEstados)
            			->with('programas', $this->comboProgramas)
            			->with('municipios', $this->comboMunicipios)
            			->with('tiposQuejas', $this->comboTiposQuejas)
            			->with('instituciones', $this->comboInstituciones);
		}
	}


	public function canceladas()
	{
		if(Auth::user()->categoria == 'Enlace'){

			$quejas = DB::table('queja')
							//->where('idEnlace', Auth::user()->id)
							->where('idEnlace', '1')
							->where('estadoQueja', 'Cancelada')
							->orderBy('updated_at', 'desc')
							->paginate(20);



			return view('enlace.quejas.canceladas')
						->with('quejas', $quejas)
						->with('estados', $this->comboEstados)
            			->with('programas', $this->comboProgramas)
            			->with('municipios', $this->comboMunicipios)
            			->with('tiposQuejas', $this->comboTiposQuejas)
            			->with('instituciones', $this->comboInstituciones);

		}
		elseif(Auth::user()->categoria == 'SuperAdministrador' || Auth::user()->categoria == 'Turnador'){

			$quejas = DB::table('queja')
							->where('idInstitucion', Auth::user()->idInstitucion)
							->where('estadoQueja', 'Cancelada')
							->orderBy('updated_at', 'desc')
							->paginate(20);


			return view('turnador.quejas.canceladas')
						->with('quejas', $quejas);
		}
	}


	public function concluidas()
	{
		if(Auth::user()->categoria == 'Enlace'){

			$quejas = DB::table('queja')
							//->where('idEnlace', Auth::user()->id)
							->where('idEnlace', '1')
							->where('estadoQueja', 'Concluida')
							->orderBy('updated_at', 'desc')
							->paginate(20);

			$quejosos = DB::table('quejoso')->get();

			$estadosCat = DB::table('catestados')->get();

			$municipiosCat = DB::table('catmunicipios')->get();

			$tipoQuejasCat = DB::table('cattipoqueja')->get();

			$institucionescat = DB::table('catinstituciones')->get();

			$programascat = DB::table('catprograma')->get();

			return view('enlace.quejas.concluidas')
						->with('municipiosCat', $municipiosCat)
						->with('estadosCat', $estadosCat)
						->with('quejas', $quejas)
						->with('quejosos', $quejosos)
						->with('catestados', $estadosCat)
						->with('catmunicipios', $municipiosCat)
						->with('cattipoquejas', $tipoQuejasCat)
						->with('catinstituciones', $institucionescat)
						->with('catprogramas', $programascat)
						->with('estados', $this->comboEstados)
            			->with('programas', $this->comboProgramas)
            			->with('municipios', $this->comboMunicipios)
            			->with('tiposQuejas', $this->comboTiposQuejas)
            			->with('instituciones', $this->comboInstituciones);

		}
		elseif(Auth::user()->categoria == 'SuperAdministrador' || Auth::user()->categoria == 'Turnador'){

			$quejas = DB::table('queja')
							->where('idInstitucion', Auth::user()->idInstitucion)
							->where('estadoQueja', 'Concluida')
							->orderBy('updated_at', 'desc')
							->paginate(20);

			return view('turnador.quejas.concluidas')
						->with('quejas', $quejas);
		}
	}


	public function rechazadas()
	{
		if(Auth::user()->categoria == 'Enlace'){

			$quejas = DB::table('queja')
							//->where('idEnlace', Auth::user()->id)
							->where('idEnlace', '1')
							->where('estadoQueja', 'Rechazada')
							->orderBy('updated_at', 'desc')
							->paginate(20);

			$quejosos = DB::table('quejoso')->get();

			$estadosCat = DB::table('catestados')->get();

			$municipiosCat = DB::table('catmunicipios')->get();

			$tipoQuejasCat = DB::table('cattipoqueja')->get();

			$institucionescat = DB::table('catinstituciones')->get();

			$programascat = DB::table('catprograma')->get();

			return view('enlace.quejas.rechazadas')
						->with('municipiosCat', $municipiosCat)
						->with('estadosCat', $estadosCat)
						->with('quejas', $quejas)
						->with('quejosos', $quejosos)
						->with('catestados', $estadosCat)
						->with('catmunicipios', $municipiosCat)
						->with('cattipoquejas', $tipoQuejasCat)
						->with('catinstituciones', $institucionescat)
						->with('catprogramas', $programascat)
						->with('estados', $this->comboEstados)
            			->with('programas', $this->comboProgramas)
            			->with('municipios', $this->comboMunicipios)
            			->with('tiposQuejas', $this->comboTiposQuejas)
            			->with('instituciones', $this->comboInstituciones);

		}
		elseif(Auth::user()->categoria == 'SuperAdministrador' || Auth::user()->categoria == 'Turnador'){

			$quejas = DB::table('queja')
							->where('idInstitucion', Auth::user()->idInstitucion)
							->where('estadoQueja', 'Rechazada')
							->orderBy('updated_at', 'desc')
							->paginate(20);

			return view('turnador.quejas.rechazadas')
						->with('quejas', $quejas);
		}
	}

	public function nuevas()
	{
		if(Auth::user()->categoria == 'Enlace'){

			$quejas = DB::table('queja')
							//->where('idEnlace', Auth::user()->id)
							->where('idEnlace', '1')
							->where('estadoQueja', 'Pendiente')
							->orderBy('updated_at', 'desc')
							->paginate(20);

			$quejosos = DB::table('quejoso')->get();

			$estadosCat = DB::table('catestados')->get();

			$municipiosCat = DB::table('catmunicipios')->get();

			$tipoQuejasCat = DB::table('cattipoqueja')->get();

			$institucionescat = DB::table('catinstituciones')->get();

			$programascat = DB::table('catprograma')->get();


			return view('enlace.quejas.nuevas')
						->with('municipiosCat', $municipiosCat)
						->with('estadosCat', $estadosCat)
						->with('quejas', $quejas)
						->with('quejosos', $quejosos)
						->with('catestados', $estadosCat)
						->with('catmunicipios', $municipiosCat)
						->with('cattipoquejas', $tipoQuejasCat)
						->with('catinstituciones', $institucionescat)
						->with('catprogramas', $programascat)
						->with('estados', $this->comboEstados)
            			->with('programas', $this->comboProgramas)
            			->with('municipios', $this->comboMunicipios)
            			->with('tiposQuejas', $this->comboTiposQuejas)
            			->with('instituciones', $this->comboInstituciones);

		}
		elseif(Auth::user()->categoria == 'SuperAdministrador' || Auth::user()->categoria == 'Turnador'){

			$quejas = DB::table('queja')
							->where('idInstitucion', Auth::user()->idInstitucion)
							->where('estadoQueja', 'Pendiente')
							->orderBy('updated_at', 'desc')
							->paginate(20);


			$quejosos = DB::table('quejoso')->get();

			$estadosCat = DB::table('catestados')->get();

			$municipiosCat = DB::table('catmunicipios')->get();

			$tipoQuejasCat = DB::table('cattipoqueja')->get();

			$institucionescat = DB::table('catinstituciones')->get();

			$programascat = DB::table('catprograma')->get();

			$abogados = DB::table('users')
							->where('categoria', 'Abogado')
							->get();

			$idAbogados = DB::table('abogado')
							->get();

			return view('turnador.quejas.nuevas')
						->with('municipiosCat', $municipiosCat)
						->with('estadosCat', $estadosCat)
						->with('quejas', $quejas)
						->with('quejosos', $quejosos)
						->with('catestados', $estadosCat)
						->with('catmunicipios', $municipiosCat)
						->with('cattipoquejas', $tipoQuejasCat)
						->with('catinstituciones', $institucionescat)
						->with('catprogramas', $programascat)
						->with('estados', $this->comboEstados)
            			->with('programas', $this->comboProgramas)
            			->with('municipios', $this->comboMunicipios)
            			->with('tiposQuejas', $this->comboTiposQuejas)
            			->with('instituciones', $this->comboInstituciones)
						->with('abogados', $abogados)
						->with('idAbogados', $idAbogados);
		}
	}

	public function busqueda(Request $request)
	{

		$id_queja= $request->input('queja');
		$abogados = DB::table('users')
							->where('categoria', 'Abogado')
							->get();

		if(Auth::user()->categoria == 'Enlace'){

			$quejas = DB::table('queja')
							->where('id', $id_queja)
							->get();

			$queja = DB::table('queja')
							->where('id', $id_queja)
							->get();

			return view('partials.busqueda')
						->with('queja', $queja)
						->with('quejas', $quejas)
						->with('abogados', $abogados)
						->with('estados', $this->comboEstados)
            			->with('programas', $this->comboProgramas)
            			->with('municipios', $this->comboMunicipios)
            			->with('tiposQuejas', $this->comboTiposQuejas)
            			->with('instituciones', $this->comboInstituciones);

		}elseif (Auth::user()->categoria == 'SuperAdministrador' || Auth::user()->categoria == 'Turnador') {

			$queja = DB::table('queja')
							->where('id', $id_queja)
							->get();

			return view('partials.busqueda')
						->with('queja', $queja)
						->with('abogados', $abogados);
		}

	}

	public function asignar(Request $request)
    {
        /*
        Descomentar par pruducción
        $idUser = $request->input('idAbogado');

        $idAbogado =DB::table('abogado')
                            ->where('idUser', $idUser)
                            ->pluck('id');
        */

        DB::table('queja')
            ->where('id', $request->input('id'))
            ->update([
                'idAbogado' => $request->input('idAbogado'), //cambiar por $idAbogado
                'estadoQueja' => 'Aceptada',
                ]);

        return redirect()->to('bandeja')->with('message', 'Asignación Exitosa');
    }


}
