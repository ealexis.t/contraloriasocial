<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use App\User;
use App\Abogado;
use App\HistoricoQueja;
use DB;

class AbogadoController extends Controller
{

	public function store(Request $request)
	{

        $rules = array(
            'username' => 'required|unique:users',
            'nombre' => 'required',
            'apPaterno' => 'required',
            'apMaterno' => 'required',
            'email' => 'required',
            'idPrograma' => 'required',
            'password' => 'required',

        );

        $messages = array(
            'username.required' =>'Este campo es requerido',
            'nombre.required' => 'Este campo es requerido',
            'apPaterno.required' => 'Este campo es requerido',
            'apMaterno.required' => 'Este campo es requerido',
            'email.required' => 'Este campo es requerido',
            'idPrograma.required' => 'Este campo es requerido',
            'password.required' => 'Este campo es requerido',
        );

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {

            return redirect()->back()
                ->withInput()
                ->withErrors($validator)
                ->with('message-error', 'Debes completar todos los campos requeridos');

        } else {

    		$username = $request->input('username');

    		$user = new User;
            $user->username  = $request->input('username');
            $user->nombre    = $request->input('nombre');
            $user->apPaterno = $request->input('apPaterno');
            $user->apMaterno = $request->input('apMaterno');
            $user->email     = $request->input('email');
            $user->categoria = 'Abogado';
            $user->password  = bcrypt($request->input('password'));
            $user->idPrograma = $request->input('idPrograma');
            $user->save();

    		return redirect()->back()->with('message', 'Miembro Agregado');
        }


	}


    public function update(Request $request)
	{
		DB::table('users')
            ->where('id', $request->input('id'))
            ->update([
            	'nombre' => $request->input('nombre'),
            	'apPaterno' => $request->input('apPaterno'),
            	'apMaterno' => $request->input('apMaterno'),
            	]);

    	return redirect()->back()->with('message', 'Informacion Actualizada');
	}


	public function delete(Request $request)
	{
		DB::table('users')
            ->where('id', $request->input('id'))
            ->delete();

    	return redirect()->back()->with('message', 'Miembro Eliminado');
	}


    public function responder(Request $request)
    {

        $idUser = Auth::user()->id;

        $abogado = DB::table('abogado')->where('idUsers', $idUser)->value('id');

        $estado = $request->input('estado');

        DB::table('queja')
            ->where('id', $request->input('id'))
            ->update([
                'estadoQueja' => $estado,
                ]);

        $historicoQueja = new HistoricoQueja;
        $historicoQueja->idAbogado = $abogado;
        $historicoQueja->idQueja = $request->id;
        $historicoQueja->estadoQueja = $request->estado;
        $historicoQueja->save();

        return redirect()->to('bandeja')->with('message', 'Informacion Actualizada');
    }

    public function descarga(Request $request){

        $pruebasFile = DB::table('pruebas')
                    ->where('idQueja', $request->idqueja)
                    ->value('rutaPrueba');

        return response()->download($pruebasFile);

    }
}
