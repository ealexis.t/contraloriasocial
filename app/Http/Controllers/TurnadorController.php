<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\CatMunicipio;
use App\CatPrograma;
use App\Abogado;
use App\Enlace;
use App\CatEstado;
use App\CatTipoQueja;
use App\Quejoso;
use App\Queja;
use App\HistoricoQueja;
use DB;

class TurnadorController extends Controller
{


    protected $comboEstados;
    protected $comboMunicipios;
    protected $comboProgramas;

    public function __construct()
    {
        $this->middleware('auth');

         // Grab the logged in user if any and set it to this property.
        // All extending classes should then have access to it.
        $this->estados    = CatEstado::all();
        $this->municipios = CatMunicipio::all();
        $this->programas   = CatPrograma::all();

        $this->comboEstados = [];
        $this->comboMunicipios = [];
        $this->comboProgramas = [];

        foreach ($this->programas as $programa) {
            $this->comboProgramas[$programa->id] = $programa->nombrePrograma;
        }

        foreach ($this->municipios as $municipio) {
            $this->comboMunicipios[$municipio->id] = $municipio->nombreMunicipio;
        }

        foreach ($this->estados as $estado) {
            $this->comboEstados[$estado->id] = $estado->nombreEstado;
        }

        // Share this property with all the views in your application.
        view()->share('comboEstados', $this->comboEstados);
        view()->share('comboMunicipios', $this->comboMunicipios);
        view()->share('comboProgramas', $this->comboProgramas);
    }

    public function equipo()
	{
        if(Auth::user()->categoria == 'SuperAdministrador' || Auth::user()->categoria == 'Turnador'){

           $abogados =DB::table('users')
                            ->where('categoria','Abogado')
                            ->get();

            $quejas =DB::table('queja')
                            ->where('estadoQueja','Aceptada')
                            ->get();

            $idAbogados = DB::table('abogado')
                            ->get();

            return view('turnador.equipo.asignaciones')
                        ->with('municipios', $this->comboMunicipios)
                        ->with('programas', $this->comboProgramas)
                        ->with('estados', $this->comboEstados)
                        ->with('idAbogados', $idAbogados)
                        ->with('abogados', $abogados)
                        ->with('quejas', $quejas);
        }
        else{
            return view('404');
        }
	}


	public function asignaciones()
	{
        if(Auth::user()->categoria == 'SuperAdministrador' || Auth::user()->categoria == 'Turnador'){

           $abogados =DB::table('users')
                            ->where('categoria','Abogado')
                            ->get();

            $quejas =DB::table('queja')
                            ->where('estadoQueja','Aceptada')
                            ->get();

            $idAbogados = DB::table('abogado')
                            ->get();

            return view('turnador.equipo.asignaciones')
                        ->with('municipios', $this->comboMunicipios)
                        ->with('programas', $this->comboProgramas)
                        ->with('estados', $this->comboEstados)
                        ->with('idAbogados', $idAbogados)
                        ->with('abogados', $abogados)
                        ->with('quejas', $quejas);
        }
        else{
            return view('404');
        }
	}


	public function miembros()
	{
        if(Auth::user()->categoria == 'SuperAdministrador' || Auth::user()->categoria == 'Turnador'){

            $abogados =DB::table('users')
                            ->where('categoria','Abogado')
                            ->get();

            return view('turnador.equipo.miembros')
                        ->with('abogados', $abogados)
                        ->with('estados', $this->comboEstados)
                        ->with('programas', $this->comboProgramas)
                        ->with('municipios', $this->comboMunicipios);
        }
        else{
            return view('404');
        }
	}

    public function asignar(Request $request)
    {

        $idUser = Auth::user()->id;

        $turnador = DB::table('turnador')->where('idUsers', $idUser)->value('id');


        DB::table('queja')
            ->where('id', $request->input('id'))
            ->update([
                'estadoQueja' => 'Aceptada',
                'idAbogado' => $request->input('idAbogado'),
                ]);

        $historicoQueja = new HistoricoQueja;
        $historicoQueja->idTurnador = $turnador;
        $historicoQueja->idQueja = $request->id;
        $historicoQueja->estadoQueja = 'Aceptada';
        $historicoQueja->idAbogado = $request->idAbogado;
        $historicoQueja->save();


        return redirect()->back()->with('message', 'Asignación Exitosa');
    }


}
