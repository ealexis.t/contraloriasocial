<?php

namespace App\Http\Controllers;

//Use models
use App\CatMunicipio;
use App\CatPrograma;
use App\Abogado;
use App\Enlace;
use App\CatEstado;
use App\CatTipoQueja;
use App\Quejoso;
use App\Queja;
use App\Pruebas;
use App\HistoricoQueja;
use DB;
use Validator;
use Storage;
use File;
use Auth;

use Illuminate\Http\Request;

use App\Http\jsonFormFulls;
use App\Http\jsonFormFulls\ErrorMessages;
use App\Http\Controllers\Controller;

class EnlaceQuejaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    private $file;
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function getMunicipios(Request $request, $id){
        if($request->ajax()){
            $municipios = CatMunicipio::municipios($id);
            return response()->json($municipios);
        }
    }

    public function getProgramas(Request $request, $id){
        if($request->ajax()){
            $programas = CatPrograma::programas($id);
            return response()->json($programas);
        }
    }

    public function create(){
        $estados    = CatEstado::all();
        $municipios = CatMunicipio::all();
        $programas   = CatPrograma::all();

        $comboEstados = [];
        $comboMunicipios = [];
        $comboProgramas = [];

        foreach ($programas as $programa) {
            $comboProgramas[$programa->id] = $programa->nombrePrograma;
        }

        foreach ($municipios as $municipio) {
            $comboMunicipios[$municipio->id] = $municipio->nombreMunicipio;
        }

        foreach ($estados as $estado) {
            $comboEstados[$estado->id] = $estado->nombreEstado;
        }

        return view('levantarQueja')
            ->with('estados', $comboEstados)
            ->with('programas', $comboProgramas)
            ->with('municipios', $comboMunicipios);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request){

        //Validate the jsonFormFull

        //Categoria estado
        //$CatPrograma = CatPrograma::find(1);

        //Categoria estado
        //$abogado = Abogado::find(1);

        $api = json_decode($request->datos, true);

        $idUser = Auth::user()->id;

        $enlace = DB::table('enlace')->where('idUsers', $idUser)->value('id');

        //return $api[0];

        //quejoso store
        $quejosos            = new Quejoso;
        $quejosos->nombre    = $api[0]['nombre'];
        $quejosos->apPaterno = $api[0]['apPaterno'];
        $quejosos->apMaterno = $api[0]['apMaterno'];
        $quejosos->calle     = $api[0]['calle'];
        $quejosos->numInt    = $api[0]['numInt'];
        $quejosos->numExt    = $api[0]['numExt'];
        $quejosos->colonia   = $api[0]['colonia'];
        $quejosos->cp        = $api[0]['cp'];
        $quejosos->email     = $api[0]['email'];
        $quejosos->telefono  = $api[0]['telefono'];
        $quejosos->idEstado  = $api[0]['id_estado'];
        $quejosos->municipio = $api[0]['id_municipio'];
        $quejosos->save();

        //queja store
        $quejas = new Queja;
        $quejas->calle      = $api[1]["calleQueja"];
        $quejas->numInt     = $api[1]["numIntQueja"];
        $quejas->numExt     = $api[1]["numExtQueja"];
        $quejas->colonia    = $api[1]["coloniaQueja"];
        $quejas->cp         = $api[1]["cpQueja"];
        $quejas->idEstado   = $api[1]["id_estadoQueja"];
        $quejas->municipio  = $api[1]["id_municipioQueja"];

        $quejas->fechaHechos    = $api[2]["date"];
        $quejas->horaAproximada = $api[2]["time"];


        $quejas->nombreFuncionario      = $api[3]["nombreFuncionario"];
        $quejas->apellidoPatFuncionario = $api[3]["apellidoPatFuncionario"];
        $quejas->apellidoMatFuncionario = $api[3]["apellidoMatFuncionario"];
        $quejas->cargoFuncionario       = $api[3]["cargoFuncionario"];
        $quejas->areaAdscripcion        = $api[3]["adscripcion"];
        $quejas->idTipoQueja            = $api[3]["id_tiposQueja"];
        $quejas->idInstitucion          = $api[3]["id_institucion"];
        $quejas->idPrograma             = $api[3]["id_programaQueja"];
        $quejas->narrativa              = $api[3]["narrativa"];
        $quejas->descripcionPruebas     = $api[3]["descripcion"];

        $quejas->idQuejoso     = $quejosos->id;
        $quejas->idEnlace      = $enlace;
        $quejas->save();

        $historicoQueja = new HistoricoQueja;
        $historicoQueja->idQueja = $quejas->id;
        $historicoQueja->idEnlace = $enlace;
        $historicoQueja->estadoQueja = 'Pendiente';
        $historicoQueja->save();

        //return $request->archivoVacio;

        if($request->archivoVacio == 'no'){

            //archivo valido?¿
            if($request->archivo->isValid()){
                //obtener archivo del formulario
                $file = $request->archivo;

                //obtener solo extension del archivo
                $extension = $file->getClientOriginalExtension();

                //Crear ruta para la institucion de procedencia
                $pathInstitucion = storage_path('app/pruebas/').$api[3]["id_institucion"];

                //Crear ruta para el programa de procedencia
                $pathPrograma = $pathInstitucion.'/'.$api[3]["id_programaQueja"];

                //Crear ruta para el caso o la queja
                $pathCasoQueja = $pathPrograma.'/'.$quejas->id;

                //nombre encriptado del archivo + extensión
                $encrypteFile = bcrypt($file).'.'.$extension;

                //mover el archivo y colocar el nuevo nombre
                $file->move($pathCasoQueja, $encrypteFile);

                //capturar la ruta
                $rutaPrueba = $pathCasoQueja.'/'.$encrypteFile;

                //Guardar en la tabla pruebas
                $routePathPruebas = new Pruebas;
                $routePathPruebas->rutaPrueba = $rutaPrueba;
                $routePathPruebas->idQueja = $quejas->id;
                $routePathPruebas->save();

            }else{
                // sending back with error message.
                return redirect()->back()->with('message', 'archivo a no valido');
            }

            return $mensaje = 'ok';
                //->with('quejosos', $quejosos)
                //->with('quejas', $quejas);

        }else{

            return $mensaje = 'sin pruebas';

        }

    }
}
