<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use DB;
use Auth;
use App\Comentario;
use App\CatMunicipio;
use App\CatPrograma;
use App\Abogado;
use App\Enlace;
use App\CatEstado;
use App\CatTipoQueja;
use App\CatInstituciones;
use App\Quejoso;
use App\Queja;

class ComentarioController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');

        $this->estados         = CatEstado::all();
        $this->municipios      = CatMunicipio::all();
        $this->programas       = CatPrograma::all();
        $this->tiposQuejas     = CatTipoQueja::all();
        $this->instituciones   = CatInstituciones::all();

        $this->comboEstados       = [];
        $this->comboMunicipios    = [];
        $this->comboProgramas     = [];
        $this->comboTiposQuejas   = [];
        $this->comboInstituciones = [];

        foreach ($this->instituciones as $institucion) {
            $this->comboInstituciones[$institucion->id] = $institucion->nombreInstitucion;
        }

        foreach ($this->tiposQuejas as $tipoQueja) {
            $this->comboTiposQuejas[$tipoQueja->id] = $tipoQueja->nombreQueja;
        }

        foreach ($this->programas as $programa) {
            $this->comboProgramas[$programa->id] = $programa->nombrePrograma;
        }

        foreach ($this->municipios as $municipio) {
            $this->comboMunicipios[$municipio->id] = $municipio->nombreMunicipio;
        }

        foreach ($this->estados as $estado) {
            $this->comboEstados[$estado->id] = $estado->nombreEstado;
        }

        // Share this property with all the views in your application.
        view()->share('comboEstados', $this->comboEstados);
        view()->share('comboMunicipios', $this->comboMunicipios);
        view()->share('comboProgramas', $this->comboProgramas);
        view()->share('comboTiposQuejas', $this->comboTiposQuejas);
        view()->share('comboInstituciones', $this->comboInstituciones);
    }

    public function index(){
        if(Auth::user()->categoria == 'Enlace'){

            $idEnlace = DB::table('enlace')->where('idUsers', Auth::user()->id)->value('id');

            $comentarios = DB::table('comentario')
                            ->where('idEnlace', $idEnlace)
                            ->orderBy('updated_at', 'desc')
                            ->paginate(20);

            $quejosos = DB::table('quejoso')->get();

            $estadosCat = DB::table('catestados')->get();

            $municipiosCat = DB::table('catmunicipios')->get();

            $tipoQuejasCat = DB::table('cattipoqueja')->get();

            $institucionescat = DB::table('catinstituciones')->get();

            $programascat = DB::table('catprograma')->get();

            return view('enlace.comentarios.comentarios')
                    ->with('municipiosCat', $municipiosCat)
                    ->with('estadosCat', $estadosCat)
                    ->with('comentarios', $comentarios)
                    ->with('quejosos', $quejosos)
                    ->with('catestados', $estadosCat)
                    ->with('catmunicipios', $municipiosCat)
                    ->with('cattipoquejas', $tipoQuejasCat)
                    ->with('catinstituciones', $institucionescat)
                    ->with('catprogramas', $programascat)
                    ->with('estados', $this->comboEstados)
                    ->with('programas', $this->comboProgramas)
                    ->with('municipios', $this->comboMunicipios)
                    ->with('tiposQuejas', $this->comboTiposQuejas)
                    ->with('instituciones', $this->comboInstituciones);
        }
    }

    public function store(Request $request){

        if(Auth::user()->categoria == 'Enlace'){

            $idUser = Auth::user()->id;

            $enlace = DB::table('enlace')->where('idUsers', $idUser)->value('id');

            //dd($request);

            $comentario                        = new Comentario;
            $comentario->idEnlace              = $enlace;
            $comentario->idInstitucion         = $request->id_institucion;
            $comentario->idPrograma            = $request->id_programaQueja;
            $comentario->descripcionComentario = $request->descripcionComentario;
            $comentario->idFuncionPublica = 24;
            $comentario->save();

            return redirect()->back()->with('message-comentario', '¡Comentario guardado exitosamente!');
        }

    }

}
