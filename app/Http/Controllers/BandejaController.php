<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\CatMunicipio;
use App\CatPrograma;
use App\Abogado;
use App\Enlace;
use App\CatEstado;
use App\CatTipoQueja;
use App\CatInstituciones;
use App\Quejoso;
use App\Queja;
use DB;


class BandejaController extends Controller
{
	protected $comboEstados;
 	protected $comboMunicipios;
 	protected $comboProgramas;
 	protected $comboTiposQuejas;
 	protected $comboInstituciones;

	public function __construct()
	{
	    $this->middleware('auth');

	     // Grab the logged in user if any and set it to this property.
	    // All extending classes should then have access to it.
	    $this->estados    	   = CatEstado::all();
	    $this->municipios 	   = CatMunicipio::all();
	    $this->programas   	   = CatPrograma::all();
	    $this->tiposQuejas     = CatTipoQueja::all();
	    $this->instituciones   = CatInstituciones::all();

	    $this->comboEstados       = [];
	    $this->comboMunicipios    = [];
	    $this->comboProgramas     = [];
	    $this->comboTiposQuejas   = [];
	    $this->comboInstituciones = [];

	    foreach ($this->instituciones as $institucion) {
	        $this->comboInstituciones[$institucion->id] = $institucion->nombreInstitucion;
	    }

	    foreach ($this->tiposQuejas as $tipoQueja) {
	        $this->comboTiposQuejas[$tipoQueja->id] = $tipoQueja->nombreQueja;
	    }

	    foreach ($this->programas as $programa) {
	        $this->comboProgramas[$programa->id] = $programa->nombrePrograma;
	    }

	    foreach ($this->municipios as $municipio) {
	        $this->comboMunicipios[$municipio->id] = $municipio->nombreMunicipio;
	    }

	    foreach ($this->estados as $estado) {
	        $this->comboEstados[$estado->id] = $estado->nombreEstado;
	    }

	    // Share this property with all the views in your application.
	    view()->share('comboEstados', $this->comboEstados);
	    view()->share('comboMunicipios', $this->comboMunicipios);
	    view()->share('comboProgramas', $this->comboProgramas);
	    view()->share('comboTiposQuejas', $this->comboTiposQuejas);
	    view()->share('comboInstituciones', $this->comboInstituciones);
	}
    public function index()
	{
		if(Auth::user()->categoria == 'Enlace'){

			$enlace = DB::table('enlace')->where('idUsers', Auth::user()->id)->value('id');//Cambiar '1' por Auth::user()->id

			$quejas = DB::table('queja')
							->where('idEnlace', $enlace)
							->whereNotIn('estadoQueja', ['Pendiente'])
							->orderBy('created_at')
							->paginate(20);

			$comentarios = DB::table('comentario')
                            ->where('idEnlace', $enlace)
                            ->orderBy('updated_at', 'desc')
                            ->paginate(20);

			//$quejosos = DB::table('quejoso')->get();

			//$estados = DB::table('catestados')->get();

			//$municipios = DB::table('catmunicipios')->get();

			//$tipoQuejas = DB::table('cattipoqueja')->get();

			//$instituciones = DB::table('catinstituciones')->get();

			//$programas = DB::table('catprograma')->get();

			$quejosos = DB::table('quejoso')->get();

			$estadosCat = DB::table('catestados')->get();

			$municipiosCat = DB::table('catmunicipios')->get();

			$tipoQuejasCat = DB::table('cattipoqueja')->get();

			$institucionescat = DB::table('catinstituciones')->get();

			$programascat = DB::table('catprograma')->get();

			$abogados = DB::table('users')
							->where('categoria', 'Abogado')
							->get();

			return view('enlace.bandeja')
				//->with('quejas', $quejas)
				->with('abogados', $abogados)
				//->with('enlace', $enlace)
				//->with('quejosos', $quejosos)
				//->with('estados', $estados)
				//->with('municipios', $municipios)
				//->with('instituciones', $instituciones)
				//->with('programas', $programas)
				//->with('tipoQuejas', $tipoQuejas)
				->with('municipiosCat', $municipiosCat)
				->with('estadosCat', $estadosCat)
				->with('quejas', $quejas)
				->with('comentarios', $comentarios)
				->with('quejosos', $quejosos)
				->with('catestados', $estadosCat)
				->with('catmunicipios', $municipiosCat)
				->with('cattipoquejas', $tipoQuejasCat)
				->with('catinstituciones', $institucionescat)
				->with('catprogramas', $programascat)
				->with('estados', $this->comboEstados)
    			->with('programas', $this->comboProgramas)
    			->with('municipios', $this->comboMunicipios)
    			->with('tiposQuejas', $this->comboTiposQuejas)
    			->with('instituciones', $this->comboInstituciones);

		}
		elseif(Auth::user()->categoria == 'Abogado'){

			$id = Auth::user()->id;//Cambiar '1' por Auth::user()->id

			$abogado = DB::table('abogado')->where('idUsers', $id)->value('id');

			$quejas = DB::table('queja')
							->where('idAbogado', $abogado)
							->where('estadoQueja', 'Aceptada')
							->orderBy('created_at', 'desc')
							->paginate(20);
			/*
			$quejaPruebas = DB::table('queja')
							->where('idAbogado', $abogado)
							->where('estadoQueja', 'Aceptada')
							->get(['id']);
			*/

			//return $quejaPruebas;

			$pruebas = DB::table('pruebas')->get();
							//->where('idQueja', $quejaPruebas)->value('rutaPrueba');

			//return $pruebas;

			return view('abogado.bandeja')
						->with('quejas', $quejas)
						->with('pruebas', $pruebas);

		}

		elseif(Auth::user()->categoria == 'SuperAdministrador' || Auth::user()->categoria == 'Turnador'){

			$quejas = DB::table('queja')
							->where('idInstitucion', Auth::user()->idInstitucion)
							->orderBy('updated_at', 'desc')
							->paginate(20);

			$quejosos = DB::table('quejoso')->get();

			$estadosCat = DB::table('catestados')->get();

			$municipiosCat = DB::table('catmunicipios')->get();

			$tipoQuejasCat = DB::table('cattipoqueja')->get();

			$institucionescat = DB::table('catinstituciones')->get();

			$programascat = DB::table('catprograma')->get();

			$abogados = DB::table('users')
							->where('categoria', 'Abogado')
							->get();

			$idAbogados = DB::table('abogado')
							->get();

			return view('turnador.bandeja')
						->with('municipiosCat', $municipiosCat)
						->with('estadosCat', $estadosCat)
						->with('quejosos', $quejosos)
						->with('catestados', $estadosCat)
						->with('catmunicipios', $municipiosCat)
						->with('cattipoquejas', $tipoQuejasCat)
						->with('catinstituciones', $institucionescat)
						->with('catprogramas', $programascat)
						->with('estados', $this->comboEstados)
		    			->with('programas', $this->comboProgramas)
		    			->with('municipios', $this->comboMunicipios)
		    			->with('tiposQuejas', $this->comboTiposQuejas)
		    			->with('instituciones', $this->comboInstituciones)
						->with('quejas', $quejas)
						->with('abogados', $abogados)
						->with('idAbogados', $idAbogados);
		}
	}

}
