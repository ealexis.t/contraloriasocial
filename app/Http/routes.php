<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/



Route::group(['middleware' => ['web']], function () {
    Route::get('quejas/municipios/{id}', 'EnlaceQuejaController@getMunicipios');
    Route::get('quejas/programas/{id}', 'EnlaceQuejaController@getProgramas');
    Route::post('enlace/queja/arch', 'EnlaceQuejaController@store');

    Route::post('abogado/descarga', 'AbogadoController@descarga');
    Route::post('enlace/comentario_crear', 'ComentarioController@store');

    Route::get('comentarios', 'ComentarioController@index');
    //Route::post('enlace/queja/file/{formData}', 'EnlaceQuejaController@store');
});

Route::group(['middleware' => 'web'], function () {

    Route::auth();

    Route::get('/', 'HomeController@index');
    Route::get('modal', 'HomeController@modal');
    Route::get('ayuda', 'AyudaController@index');

/*
|--------------------------------------------------------------------------
| Routes TurnadorController
|--------------------------------------------------------------------------
*/
	Route::get('bandeja', 'BandejaController@index');
/*
|--------------------------------------------------------------------------
| Routes QuejasController
|--------------------------------------------------------------------------
*/
	Route::get('nuevas', 'QuejasController@nuevas');
	Route::get('abiertas', 'QuejasController@abiertas');
    Route::get('aceptadas', 'QuejasController@aceptadas');
	Route::any('asignar-queja', 'QuejasController@asignar');
	Route::any('busqueda', 'QuejasController@busqueda');
	Route::get('canceladas', 'QuejasController@canceladas');
	Route::get('concluidas', 'QuejasController@concluidas');
	Route::get('rechazadas', 'QuejasController@rechazadas');

/*
|--------------------------------------------------------------------------
| Routes TurnadorController
|--------------------------------------------------------------------------
*/
	Route::get('equipo', 'TurnadorController@equipo');
	Route::get('equipo/asignaciones', 'TurnadorController@asignaciones');
	Route::get('equipo/miembros', 'TurnadorController@miembros');
	Route::any('asignar', 'TurnadorController@asignar');
/*
|--------------------------------------------------------------------------
| Routes AbogadoController
|--------------------------------------------------------------------------
*/
	Route::post('create_abogado', 'AbogadoController@store');
	Route::any('update_abogado', 'AbogadoController@update');
	Route::any('delete_abogado', 'AbogadoController@delete');
	Route::any('responder', 'AbogadoController@responder');

/*
|--------------------------------------------------------------------------
| Routes resources Controlleres
|--------------------------------------------------------------------------
*/
	Route::resource('quejas', 'QuejasController');
    //Route::resource('enlace', 'EnlaceController');
    Route::resource('turnador', 'TurnadorController');
    //Route::get('enlace', 'EnlaceQuejaController@create');
    //Route::get('enlace/index', 'EnlaceQuejaController@index');

});
