<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pruebas extends Model
{
    //Use table own
    protected $table = 'pruebas';

    protected $fillable = [
        'rutaPruebas',
        'idQueja',
    ];
}
