<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatPrograma extends Model
{
    //Use table own
    protected $table = 'catprograma';

    protected $fillable = [
        'idInstitucion',
        'nombrePrograma',
        'rutaImagen',
    ];

    public static function programas($id){

        return CatPrograma::where('idInstitucion', '=', $id)->get();

    }
}
