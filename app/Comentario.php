<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    //Use table own
    protected $table = 'comentario';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'descripcionComentario',
        'visto',
        'idEnlace',
        'idFuncionPublica',
        'idInstitucion',
        'idPrograma',
    ];
}
