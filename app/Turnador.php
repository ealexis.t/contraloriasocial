<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Turnador extends Model
{
    //Use table own
    protected $table = 'turnador';
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'idUsers',
    ];
}
