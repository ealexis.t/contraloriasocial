//remove empty spaces in quejoso array[0]
        $api[0]["nombre"]           = trim($api[0]["nombre"]);
        $api[0]["apPaterno"]        = trim($api[0]["apPaterno"]);
        $api[0]["apMaterno"]        = trim($api[0]["apMaterno"]);
        $api[0]["calle"]            = trim($api[0]["calle"]);
        $api[0]["numInt"]           = trim($api[0]["numExt"]);
        $api[0]["numExt"]           = trim($api[0]["numExt"]);
        $api[0]["colonia"]          = trim($api[0]["colonia"]);
        $api[0]["cp"]               = trim($api[0]["cp"]);
        $api[0]["id_estado"]        = trim($api[0]["id_estado"]);
        $api[0]["id_municipio"]     = trim($api[0]["id_municipio"]);
        $api[0]["telefono"]         = trim($api[0]["telefono"]);
        $api[0]["email"]            = trim($api[0]["email"]);

        //remove empty spaces in queja array[1]
        $api[1]["calleQueja"]                 = trim($api[1]["calleQueja"]);
        $api[1]["numExtQueja"]                = trim($api[1]["numExtQueja"]);
        $api[1]["calleQueja"]                 = trim($api[1]["calleQueja"]);
        $api[1]["coloniaQueja"]               = trim($api[1]["coloniaQueja"]);
        $api[1]["cpQueja"]                    = trim($api[1]["cpQueja"]);
        $api[1]["id_estadoQueja"]             = trim($api[1]["id_estadoQueja"]);
        $api[1]["id_municipioQueja"]          = trim($api[1]["id_municipioQueja"]);

        //remove empty spaces in queja array[2]
        $api[2]["date"]                       = trim($api[2]["date"]);
        $api[2]["time"]                       = trim($api[2]["time"]);

        //remove empty spaces in queja array[3]
        $api[3]["nombreFuncionario"]          = trim($api[3]["nombreFuncionario"]);
        $api[3]["apellidoPatFuncionario"]     = trim($api[3]["apellidoPatFuncionario"]);
        $api[3]["apellidoMatFuncionario"]     = trim($api[3]["apellidoMatFuncionario"]);
        $api[3]["cargoFuncionario"]           = trim($api[3]["cargoFuncionario"]);
        $api[3]["adscripcion"]                = trim($api[3]["adscripcion"]);
        $api[3]["id_tiposQueja"]              = trim($api[3]["id_tiposQueja"]);
        $api[3]["id_institucion"]             = trim($api[3]["id_institucion"]);
        $api[3]["id_programaQueja"]           = trim($api[3]["id_programaQueja"]);
        $api[3]["narrativa"]                  = trim($api[3]["narrativa"]);
        $api[3]["descripcion"]                = trim($api[3]["descripcion"]);


        //Variables de decisiones
        $nombreQ       = '';
        $apPaternoQ    = '';
        $apMaternoQ    = '';
        $calleQ        = '';
        $numIntQ       = '';
        $numExtQ       = '';
        $coloniaQ      = '';
        $cpQ           = '';
        $id_estadoQ    = '';
        $id_municipioQ = '';
        $telefonoQ     = '';
        $emailQ        = '';
        $mensaje       = '';


        /*array[0]*/
        //Verifica nombre
        if($api[0]["nombre"] == ''){
            $nombreQ = 'Vacio';
        }elseif(!($api[0]["nombre"] == '')){
            $nombreQ = $api[0]["nombre"];
        }

        //Verifica apellido paterno
        if($api[0]["apPaterno"] == ''){
            $apPaternoQ = 'Vacio';
        }elseif (!($api[0]["apPaterno"] == '')) {
            $apPaternoQ = $api[0]["apPaterno"];
        }

        //Verifica apellido materno
        if($api[0]["apMaterno"] == ''){
            $apMaternoQ = 'Vacio';
        }elseif (!($api[0]["apMaterno"] == '')) {
            $apMaternoQ = $api[0]["apMaterno"];
        }

        //Verifica calle
        if($api[0]["calle"] == ''){
            $calleQ = 'Vacio';
        }elseif (!($api[0]["calle"] == '')) {
            $calleQ = $api[0]["calle"];
        }

        //Verifica numero interno
        if($api[0]["numInt"] == ''){
            $numIntQ = 'Vacio';
        }elseif (!($api[0]["numInt"] == '')) {
            $numIntQ = $api[0]["numInt"];
        }

        //Verifica numero externo
        if($api[0]["numExt"] == ''){
            $numExtQ = 'Vacio';
        }elseif (!($api[0]["numExt"] == '')) {
            $numExtQ = $api[0]["numExt"];
        }

        //Verifica colonia
        if($api[0]["colonia"] == ''){
            $coloniaQ = 'Vacio';
        }elseif (!($api[0]["colonia"] == '')) {
            $coloniaQ = $api[0]["colonia"];
        }

        //Verifica cp
        if($api[0]["cp"] == ''){
            $cpQ = 'Vacio';
        }elseif (!($api[0]["cp"] == '')) {
            $cpQ = $api[0]["cp"];
        }

        //Verifica estado
        if($api[0]["id_estado"] == '0'){
            $id_estadoQ = 33;
        }elseif (!$api[0]["id_estado"] == '0') {
            $id_estadoQ = $api[0]["id_estado"];
        }

        //Verifica municipio
        if($api[0]["id_municipio"] == "placeholder"){
            $id_municipioQ = '0';
        }elseif (!($api[0]["id_municipio"] == 'placeholder')) {
            $id_municipioQ = $api[0]["id_municipio"];
        }

        //Verifica telefono
        if($api[0]["telefono"] == ''){
            $telefonoQ = 'Vacio';
        }elseif (!($api[0]["telefono"] == '')) {
            $telefonoQ = $api[0]["telefono"];
        }

        //Verifica email
        if($api[0]["email"] == ''){
            $emailQ = 'Vacio@Vacio';
        }elseif (!($api[0]["email"] == '')) {
            $emailQ = $api[0]["email"];
        }

        /*
            return dd(array(
                $nombreQ,//0
                $apPaternoQ,//1
                $apMaternoQ,//2
                $calleQ,//3
                $numIntQ,//4
                $numExtQ,//5
                $coloniaQ,//6
                $cpQ,//7
                $id_estadoQ,//8
                $id_municipioQ,//9
                $telefonoQ,//10
                $emailQ,//11
            ));
        */

            if(!($request->anonimo == 'on')){

                //No anonimo
                if(($nombreQ != 'Vacio') && ($apPaternoQ != 'Vacio') && ($apMaternoQ != 'Vacio')){

                    //quejoso store
                    $quejosos            = new Quejoso;
                    $quejosos->nombre    = $nombreQ;
                    $quejosos->apPaterno = $apPaternoQ;
                    $quejosos->apMaterno = $apMaternoQ;
                    $quejosos->calle     = $calleQ;
                    $quejosos->numInt    = $numIntQ;
                    $quejosos->numExt    = $numExtQ;
                    $quejosos->colonia   = $coloniaQ;
                    $quejosos->cp        = $cpQ;
                    $quejosos->idEstado  = $id_estadoQ;
                    $quejosos->municipio = $id_municipioQ;
                    $quejosos->telefono  = $telefonoQ;
                    $quejosos->email     = $emailQ;
                    $quejosos->save();

                    //queja store
                    $quejas = new Queja;
                    $quejas->calle      = $api[1]["calleQueja"];
                    $quejas->numInt     = $api[1]["numIntQueja"];
                    $quejas->numExt     = $api[1]["numExtQueja"];
                    $quejas->colonia    = $api[1]["coloniaQueja"];
                    $quejas->cp         = $api[1]["cpQueja"];
                    $quejas->idEstado   = $api[1]["id_estadoQueja"];
                    $quejas->municipio  = $api[1]["id_municipioQueja"];

                    $quejas->fechaHechos    = $api[2]["date"];
                    $quejas->horaAproximada = $api[2]["time"];


                    $quejas->nombreFuncionario      = $api[3]["nombreFuncionario"];
                    $quejas->apellidoPatFuncionario = $api[3]["apellidoPatFuncionario"];
                    $quejas->apellidoMatFuncionario = $api[3]["apellidoMatFuncionario"];
                    $quejas->cargoFuncionario       = $api[3]["cargoFuncionario"];
                    $quejas->areaAdscripcion        = $api[3]["adscripcion"];
                    $quejas->idTipoQueja            = $api[3]["id_tiposQueja"];
                    $quejas->idInstitucion          = $api[3]["id_institucion"];
                    $quejas->idPrograma             = $api[3]["id_programaQueja"];
                    $quejas->narrativa              = $api[3]["narrativa"];
                    $quejas->descripcionPruebas     = $api[3]["descripcion"];

                    $quejas->idQuejoso     = $quejosos->id;
                    $quejas->idEnlace      = $enlace;

                    $quejas->save();

                    //archivo valido?¿
                    if($request->archivo->isValid()){
                        //obtener archivo del formulario
                        $file = $request->archivo;

                        //obtener solo extension del archivo
                        $extension = $file->getClientOriginalExtension();

                        //Crear ruta para el programa
                        $pathPrograma = storage_path('app/pruebas/').$request->id_programaQueja;

                        //Crear ruta para el caso o la queja
                        $pathCasoQueja = $pathPrograma.'/'.$quejas->id;

                        //nombre encriptado del archivo + extensión
                        $encrypteFile = bcrypt($file).'.'.$extension;

                        //mover el archivo y colocar el nuevo nombre
                        $file->move($pathCasoQueja, $encrypteFile);

                        //capturar la ruta
                        $rutaPrueba = $pathCasoQueja.'/'.$encrypteFile;

                        //Guardar en la tabla pruebas
                        $routePathPruebas = new Pruebas;
                        $routePathPruebas->rutaPrueba = $rutaPrueba;
                        $routePathPruebas->idQueja = $quejas->id;
                        $routePathPruebas->save();

                    }else{
                        // sending back with error message.
                        return redirect()->back()->with('message', 'archivo a no valido');
                    }

                    $mensajey = 'Se puede localizar por nombre completo';

                    }elseif(($calleQ != 'Vacio') && ($numExtQ != 'Vacio') && ($numIntQ != 'Vacio') && ($coloniaQ != 'Vacio') && ($cpQ != 'Vacio') && ($id_estadoQ != '0') && ($id_municipioQ != '0')){

                        //quejoso store
                        $quejosos            = new Quejoso;
                        $quejosos->nombre    = $nombreQ;
                        $quejosos->apPaterno = $apPaternoQ;
                        $quejosos->apMaterno = $apMaternoQ;
                        $quejosos->calle     = $calleQ;
                        $quejosos->numInt    = $numIntQ;
                        $quejosos->numExt    = $numExtQ;
                        $quejosos->colonia   = $coloniaQ;
                        $quejosos->cp        = $cpQ;
                        $quejosos->municipio = $id_municipioQ;
                        $quejosos->telefono  = $telefonoQ;
                        $quejosos->email     = $emailQ;
                        $quejosos->idEstado  = $id_estadoQ;
                        $quejosos->save();

                        //queja store
                        $quejas = new Queja;
                        $quejas->calle      = $api[1]["calleQueja"];
                        $quejas->numInt     = $api[1]["numIntQueja"];
                        $quejas->numExt     = $api[1]["numExtQueja"];
                        $quejas->colonia    = $api[1]["coloniaQueja"];
                        $quejas->cp         = $api[1]["cpQueja"];
                        $quejas->idEstado   = $api[1]["id_estadoQueja"];
                        $quejas->municipio  = $api[1]["id_municipioQueja"];

                        $quejas->fechaHechos    = $api[2]["date"];
                        $quejas->horaAproximada = $api[2]["time"];


                        $quejas->nombreFuncionario      = $api[3]["nombreFuncionario"];
                        $quejas->apellidoPatFuncionario = $api[3]["apellidoPatFuncionario"];
                        $quejas->apellidoMatFuncionario = $api[3]["apellidoMatFuncionario"];
                        $quejas->cargoFuncionario       = $api[3]["cargoFuncionario"];
                        $quejas->areaAdscripcion        = $api[3]["adscripcion"];
                        $quejas->idTipoQueja            = $api[3]["id_tiposQueja"];
                        $quejas->idInstitucion          = $api[3]["id_institucion"];
                        $quejas->idPrograma             = $api[3]["id_programaQueja"];
                        $quejas->narrativa              = $api[3]["narrativa"];
                        $quejas->descripcionPruebas     = $api[3]["descripcion"];

                        $quejas->idQuejoso     = $quejosos->id;
                        $quejas->idEnlace      = $enlace;

                        $quejas->save();

                        //archivo valido?¿
                        if($request->file('archivo')->isValid()){
                            //obtener archivo del formulario
                            $file = $request->file('archivo');

                            //obtener solo extension del archivo
                            $extension = $file->getClientOriginalExtension();

                            //Crear ruta para el programa
                            $pathPrograma = storage_path('app/pruebas/').$request->id_programaQueja;

                            //Crear ruta para el caso o la queja
                            $pathCasoQueja = $pathPrograma.'/'.$quejas->id;

                            //nombre encriptado del archivo + extensión
                            $encrypteFile = bcrypt($file).'.'.$extension;

                            //mover el archivo y colocar el nuevo nombre
                            $file->move($pathCasoQueja, $encrypteFile);

                            //capturar la ruta
                            $rutaPrueba = $pathCasoQueja.'/'.$encrypteFile;

                            //Guardar en la tabla pruebas
                            $routePathPruebas = new Pruebas;
                            $routePathPruebas->rutaPrueba = $rutaPrueba;
                            $routePathPruebas->idQueja = $quejas->id;
                            $routePathPruebas->save();

                        }else{
                            // sending back with error message.
                            return redirect()->back()->with('message', 'archivo a no valido');
                        }

                        $mensaje = 'Se puede contactar mediante domicilio';

                    }elseif($telefonoQ != 'Vacio'){

                        //quejoso store
                        $quejosos            = new Quejoso;
                        $quejosos->nombre    = $nombreQ;
                        $quejosos->apPaterno = $apPaternoQ;
                        $quejosos->apMaterno = $apMaternoQ;
                        $quejosos->calle     = $calleQ;
                        $quejosos->numInt    = $numIntQ;
                        $quejosos->numExt    = $numExtQ;
                        $quejosos->colonia   = $coloniaQ;
                        $quejosos->cp        = $cpQ;
                        $quejosos->municipio = $id_municipioQ;
                        $quejosos->telefono  = $telefonoQ;
                        $quejosos->email     = $emailQ;
                        $quejosos->idEstado  = $id_estadoQ;
                        $quejosos->save();

                        //queja store
                        $quejas = new Queja;
                        $quejas->calle      = $api[1]["calleQueja"];
                        $quejas->numInt     = $api[1]["numIntQueja"];
                        $quejas->numExt     = $api[1]["numExtQueja"];
                        $quejas->colonia    = $api[1]["coloniaQueja"];
                        $quejas->cp         = $api[1]["cpQueja"];
                        $quejas->idEstado   = $api[1]["id_estadoQueja"];
                        $quejas->municipio  = $api[1]["id_municipioQueja"];

                        $quejas->fechaHechos    = $api[2]["date"];
                        $quejas->horaAproximada = $api[2]["time"];


                        $quejas->nombreFuncionario      = $api[3]["nombreFuncionario"];
                        $quejas->apellidoPatFuncionario = $api[3]["apellidoPatFuncionario"];
                        $quejas->apellidoMatFuncionario = $api[3]["apellidoMatFuncionario"];
                        $quejas->cargoFuncionario       = $api[3]["cargoFuncionario"];
                        $quejas->areaAdscripcion        = $api[3]["adscripcion"];
                        $quejas->idTipoQueja            = $api[3]["id_tiposQueja"];
                        $quejas->idInstitucion          = $api[3]["id_institucion"];
                        $quejas->idPrograma             = $api[3]["id_programaQueja"];
                        $quejas->narrativa              = $api[3]["narrativa"];
                        $quejas->descripcionPruebas     = $api[3]["descripcion"];

                        $quejas->idQuejoso     = $quejosos->id;
                        $quejas->idEnlace      = $enlace;

                        $quejas->save();

                        //archivo valido?¿
                        if($request->file('archivo')->isValid()){
                            //obtener archivo del formulario
                            $file = $request->file('archivo');

                            //obtener solo extension del archivo
                            $extension = $file->getClientOriginalExtension();

                            //Crear ruta para el programa
                            $pathPrograma = storage_path('app/pruebas/').$request->id_programaQueja;

                            //Crear ruta para el caso o la queja
                            $pathCasoQueja = $pathPrograma.'/'.$quejas->id;

                            //nombre encriptado del archivo + extensión
                            $encrypteFile = bcrypt($file).'.'.$extension;

                            //mover el archivo y colocar el nuevo nombre
                            $file->move($pathCasoQueja, $encrypteFile);

                            //capturar la ruta
                            $rutaPrueba = $pathCasoQueja.'/'.$encrypteFile;

                            //Guardar en la tabla pruebas
                            $routePathPruebas = new Pruebas;
                            $routePathPruebas->rutaPrueba = $rutaPrueba;
                            $routePathPruebas->idQueja = $quejas->id;
                            $routePathPruebas->save();

                        }else{
                            // sending back with error message.
                            return redirect()->back()->with('message', 'archivo a no valido');
                        }

                        $mensaje = 'Se puede contactar via telefonica';

                    }elseif($emailQ != 'Vacio'){

                        //quejoso store
                        $quejosos            = new Quejoso;
                        $quejosos->nombre    = $nombreQ;
                        $quejosos->apPaterno = $apPaternoQ;
                        $quejosos->apMaterno = $apMaternoQ;
                        $quejosos->calle     = $calleQ;
                        $quejosos->numInt    = $numIntQ;
                        $quejosos->numExt    = $numExtQ;
                        $quejosos->colonia   = $coloniaQ;
                        $quejosos->cp        = $cpQ;
                        $quejosos->municipio = $id_municipioQ;
                        $quejosos->telefono  = $telefonoQ;
                        $quejosos->email     = $emailQ;
                        $quejosos->idEstado  = $id_estadoQ;
                        $quejosos->save();

                        //queja store
                        $quejas = new Queja;
                        $quejas->calle      = $api[1]["calleQueja"];
                        $quejas->numInt     = $api[1]["numIntQueja"];
                        $quejas->numExt     = $api[1]["numExtQueja"];
                        $quejas->colonia    = $api[1]["coloniaQueja"];
                        $quejas->cp         = $api[1]["cpQueja"];
                        $quejas->idEstado   = $api[1]["id_estadoQueja"];
                        $quejas->municipio  = $api[1]["id_municipioQueja"];

                        $quejas->fechaHechos    = $api[2]["date"];
                        $quejas->horaAproximada = $api[2]["time"];


                        $quejas->nombreFuncionario      = $api[3]["nombreFuncionario"];
                        $quejas->apellidoPatFuncionario = $api[3]["apellidoPatFuncionario"];
                        $quejas->apellidoMatFuncionario = $api[3]["apellidoMatFuncionario"];
                        $quejas->cargoFuncionario       = $api[3]["cargoFuncionario"];
                        $quejas->areaAdscripcion        = $api[3]["adscripcion"];
                        $quejas->idTipoQueja            = $api[3]["id_tiposQueja"];
                        $quejas->idInstitucion          = $api[3]["id_institucion"];
                        $quejas->idPrograma             = $api[3]["id_programaQueja"];
                        $quejas->narrativa              = $api[3]["narrativa"];
                        $quejas->descripcionPruebas     = $api[3]["descripcion"];

                        $quejas->idQuejoso     = $quejosos->id;
                        $quejas->idEnlace      = $enlace;

                        $quejas->save();

                        //archivo valido?¿
                        if($request->file('archivo')->isValid()){
                            //obtener archivo del formulario
                            $file = $request->file('archivo');

                            //obtener solo extension del archivo
                            $extension = $file->getClientOriginalExtension();

                            //Crear ruta para el programa
                            $pathPrograma = storage_path('app/pruebas/').$request->id_programaQueja;

                            //Crear ruta para el caso o la queja
                            $pathCasoQueja = $pathPrograma.'/'.$quejas->id;

                            //nombre encriptado del archivo + extensión
                            $encrypteFile = bcrypt($file).'.'.$extension;

                            //mover el archivo y colocar el nuevo nombre
                            $file->move($pathCasoQueja, $encrypteFile);

                            //capturar la ruta
                            $rutaPrueba = $pathCasoQueja.'/'.$encrypteFile;

                            //Guardar en la tabla pruebas
                            $routePathPruebas = new Pruebas;
                            $routePathPruebas->rutaPrueba = $rutaPrueba;
                            $routePathPruebas->idQueja = $quejas->id;
                            $routePathPruebas->save();

                        }else{
                            // sending back with error message.
                            return redirect()->back()->with('message', 'archivo a no valido');
                        }

                        $mensaje = 'Se puede contactar via correo';

                    }else{

                        //quejoso store
                        $quejosos            = new Quejoso;
                        $quejosos->nombre    = 'Anonimo';
                        $quejosos->apPaterno = 'Anonimo';
                        $quejosos->apMaterno = 'Anonimo';
                        $quejosos->calle     = 'Anonimo';
                        $quejosos->numInt    = 'S/N';
                        $quejosos->numExt    = 'S/N';
                        $quejosos->colonia   = 'Anonimo';
                        $quejosos->cp        = 'S/CP';
                        $quejosos->municipio = '0';
                        $quejosos->telefono  = 'Anonimo';
                        $quejosos->email     = 'Anonimo@Anonimo';
                        $quejosos->idEstado  = '1';
                        $quejosos->save();

                        //queja store
                        $quejas = new Queja;
                        $quejas->calle      = $api[1]["calleQueja"];
                        $quejas->numInt     = $api[1]["numIntQueja"];
                        $quejas->numExt     = $api[1]["numExtQueja"];
                        $quejas->colonia    = $api[1]["coloniaQueja"];
                        $quejas->cp         = $api[1]["cpQueja"];
                        $quejas->idEstado   = $api[1]["id_estadoQueja"];
                        $quejas->municipio  = $api[1]["id_municipioQueja"];

                        $quejas->fechaHechos    = $api[2]["date"];
                        $quejas->horaAproximada = $api[2]["time"];


                        $quejas->nombreFuncionario      = $api[3]["nombreFuncionario"];
                        $quejas->apellidoPatFuncionario = $api[3]["apellidoPatFuncionario"];
                        $quejas->apellidoMatFuncionario = $api[3]["apellidoMatFuncionario"];
                        $quejas->cargoFuncionario       = $api[3]["cargoFuncionario"];
                        $quejas->areaAdscripcion        = $api[3]["adscripcion"];
                        $quejas->idTipoQueja            = $api[3]["id_tiposQueja"];
                        $quejas->idInstitucion          = $api[3]["id_institucion"];
                        $quejas->idPrograma             = $api[3]["id_programaQueja"];
                        $quejas->narrativa              = $api[3]["narrativa"];
                        $quejas->descripcionPruebas     = $api[3]["descripcion"];

                        $quejas->idQuejoso     = $quejosos->id;
                        $quejas->idEnlace      = $enlace;

                        $quejas->save();

                        //archivo valido?¿
                        if($request->file('archivo')->isValid()){
                            //obtener archivo del formulario
                            $file = $request->file('archivo');

                            //obtener solo extension del archivo
                            $extension = $file->getClientOriginalExtension();

                            //Crear ruta para el programa
                            $pathPrograma = storage_path('app/pruebas/').$request->id_programaQueja;

                            //Crear ruta para el caso o la queja
                            $pathCasoQueja = $pathPrograma.'/'.$quejas->id;

                            //nombre encriptado del archivo + extensión
                            $encrypteFile = bcrypt($file).'.'.$extension;

                            //mover el archivo y colocar el nuevo nombre
                            $file->move($pathCasoQueja, $encrypteFile);

                            //capturar la ruta
                            $rutaPrueba = $pathCasoQueja.'/'.$encrypteFile;

                            //Guardar en la tabla pruebas
                            $routePathPruebas = new Pruebas;
                            $routePathPruebas->rutaPrueba = $rutaPrueba;
                            $routePathPruebas->idQueja = $quejas->id;
                            $routePathPruebas->save();

                        }else{
                            // sending back with error message.
                            return redirect()->back()->with('message', 'archivo a no valido');
                        }

                        $mensaje = 'Denuncia Anonima';

                    }

            }elseif($api[0]['anonimo'] == 'on'){

                //quejoso store
                $quejosos            = new Quejoso;
                $quejosos->nombre    = 'Anonimo';
                $quejosos->apPaterno = 'Anonimo';
                $quejosos->apMaterno = 'Anonimo';
                $quejosos->calle     = 'Anonimo';
                $quejosos->numInt    = 'S/N';
                $quejosos->numExt    = 'S/N';
                $quejosos->colonia   = 'Anonimo';
                $quejosos->cp        = 'S/CP';
                $quejosos->municipio = '0';
                $quejosos->telefono  = 'Anonimo';
                $quejosos->email     = 'Anonimo@Anonimo';
                $quejosos->idEstado  = '1';
                //hay que agregar esta madre a la base de datos idEstado = 0
                $quejosos->save();

                //queja store
                $quejas = new Queja;
                $quejas->calle      = $api[1]["calleQueja"];
                $quejas->numInt     = $api[1]["numIntQueja"];
                $quejas->numExt     = $api[1]["numExtQueja"];
                $quejas->colonia    = $api[1]["coloniaQueja"];
                $quejas->cp         = $api[1]["cpQueja"];
                $quejas->idEstado   = $api[1]["id_estadoQueja"];
                $quejas->municipio  = $api[1]["id_municipioQueja"];

                $quejas->fechaHechos    = $api[2]["date"];
                $quejas->horaAproximada = $api[2]["time"];


                $quejas->nombreFuncionario      = $api[3]["nombreFuncionario"];
                $quejas->apellidoPatFuncionario = $api[3]["apellidoPatFuncionario"];
                $quejas->apellidoMatFuncionario = $api[3]["apellidoMatFuncionario"];
                $quejas->cargoFuncionario       = $api[3]["cargoFuncionario"];
                $quejas->areaAdscripcion        = $api[3]["adscripcion"];
                $quejas->idTipoQueja            = $api[3]["id_tiposQueja"];
                $quejas->idInstitucion          = $api[3]["id_institucion"];
                $quejas->idPrograma             = $api[3]["id_programaQueja"];
                $quejas->narrativa              = $api[3]["narrativa"];
                $quejas->descripcionPruebas     = $api[3]["descripcion"];

                $quejas->idQuejoso     = $quejosos->id;
                $quejas->idEnlace      = $enlace;

                $quejas->save();

                //archivo valido?¿

                if($request->file('archivo')->isValid()){
                    //obtener archivo del formulario
                    $file = $request->file('archivo');

                    //obtener solo extension del archivo
                    $extension = $file->getClientOriginalExtension();

                    //Crear ruta para el programa
                    $pathPrograma = storage_path('app/pruebas/').$request->id_programaQueja;

                    //Crear ruta para el caso o la queja
                    $pathCasoQueja = $pathPrograma.'/'.$quejas->id;

                    //nombre encriptado del archivo + extensión
                    $encrypteFile = bcrypt($file).'.'.$extension;

                    //mover el archivo y colocar el nuevo nombre
                    $file->move($pathCasoQueja, $encrypteFile);

                    //capturar la ruta
                    $rutaPrueba = $pathCasoQueja.'/'.$encrypteFile;

                    //Guardar en la tabla pruebas
                    $routePathPruebas = new Pruebas;
                    $routePathPruebas->rutaPrueba = $rutaPrueba;
                    $routePathPruebas->idQueja = $quejas->id;
                    $routePathPruebas->save();

                }else{
                    // sending back with error message.
                    return redirect()->back()->with('message', 'archivo a no valido');
                }

                $mensaje = 'Denuncia Anonimo por checkbox';

            }
