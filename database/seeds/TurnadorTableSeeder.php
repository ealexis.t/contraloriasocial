<?php

use Illuminate\Database\Seeder;

class TurnadorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = DB::table('users')->where('categoria', '=', 'Turnador')->get();

        foreach ($users as $user) {

            if($user->categoria == 'Turnador'){

                DB::table('turnador')->insert([

                    'idUsers' => $user->id,

                ]);

            }

        }
    }
}
