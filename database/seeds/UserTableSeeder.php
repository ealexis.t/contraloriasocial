<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(App\User::class)->create([
            'id'        => '1',
            'username'  => 'Fachi',
            'password'  => bcrypt('admin'),
            'categoria' => 'SuperAdministrador',
            'email'     => 'phachi3@gmail.com',
            'nombre'    => 'Fausto',
            'apPaterno' => 'Fernández',
            'apMaterno' => 'Hernández',
            'idInstitucion' => '10',
            'idEstados' => '9',
            ]);



        factory(App\User::class)->create([
            'id'        =>  '2',
            'username'  => 'alexis',
            'password'  => bcrypt('admin'),
            'categoria' => 'SuperAdministrador',
            'email'     => 'ealexis.t@gmail.com',
            'nombre'    => 'Alexis',
            'apPaterno' => 'Tapia',
            'apMaterno' => 'Hipolito',
            'idInstitucion' => '10',
            'idEstados' => '9',
            ]);

        //Crear el enlace
        factory(App\User::class)->create([
            'id'        =>  '3',
            'username'  => 'Enlace',
            'password'  => bcrypt('123'),
            'categoria' => 'Enlace',
            'email'     => 'enlace@gob.mx',
            //'nombre'    => 'Fausto',
            //'apPaterno' => 'Fernández',
            //'apMaterno' => 'Hernández',
            'idInstitucion' => '10',
            'idEstados' => '9',
            ]);

        //asignar istitución
        $j = 1;

        //Crear los turnadores
        for($i=4; $i<=13; $i++){
            factory(App\User::class)->create([
            'id'        => $i,
            'username'  => 'Turnador'.$j,
            'password'  => bcrypt('123'),
            'categoria' => 'Turnador',
            'email'     => 'turnador@gob.mx',
            //'nombre'    => 'Fausto',
            //'apPaterno' => 'Fernández',
            //'apMaterno' => 'Hernández',
            'idInstitucion' => $j,
            'idEstados' => '9',
            ]);
            $j++;
        }

        //asignar istitución
        $j = 1;

        //Crear los abogados
        for($i=14; $i<=23; $i++){
            factory(App\User::class)->create([
            'id'        => $i,
            'username'  => 'Abogado'.$j,
            'password'  => bcrypt('123'),
            'categoria' => 'Abogado',
            'email'     => 'abogado@gob.mx',
            //'nombre'    => 'Fausto',
            //'apPaterno' => 'Fernández',
            //'apMaterno' => 'Hernández',
            'idInstitucion' => $j,
            'idEstados' => '9',
            ]);
            $j++;
        }

        factory(App\User::class)->create([
            'username'  => 'FuncionPublica',
            'password'  => bcrypt('123'),
            'categoria' => 'FunciónPublica',
            'email'     => 'funcionpublica@gob.com',
            //'nombre'    => 'Fausto',
            //'apPaterno' => 'Fernández',
            //'apMaterno' => 'Hernández',
            'idInstitucion' => '10',
            'idEstados' => '9',
            ]);
    }
}
