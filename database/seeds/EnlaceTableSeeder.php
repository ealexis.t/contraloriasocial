<?php

use Illuminate\Database\Seeder;

class EnlaceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = DB::table('users')->where('categoria', '=', 'Enlace')->get();

        foreach ($users as $user) {

            if($user->categoria == 'Enlace'){

                DB::table('enlace')->insert([

                    'idUsers' => $user->id,

                ]);

            }

        }
    }
}
