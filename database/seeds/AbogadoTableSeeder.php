<?php

use Illuminate\Database\Seeder;

class AbogadoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = DB::table('users')->where('categoria', '=', 'Abogado')->get();

        foreach ($users as $user) {

            if($user->categoria == 'Abogado'){

                DB::table('abogado')->insert([

                    'idUsers' => $user->id,

                ]);

            }

        }
    }
}
