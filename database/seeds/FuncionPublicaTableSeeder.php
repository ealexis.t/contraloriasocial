<?php

use Illuminate\Database\Seeder;

class FuncionPublicaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = DB::table('users')->where('categoria', '=', 'FunciónPublica')->get();

        foreach ($users as $user) {

            if($user->categoria == 'FunciónPublica'){

                DB::table('funcionpublica')->insert([

                    'idUsers' => $user->id,

                ]);

            }

        }
    }
}
