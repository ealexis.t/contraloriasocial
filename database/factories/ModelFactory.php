<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/*
$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'username'       => $faker->userName,
        'password'       => bcrypt('user'),
        'categoria'      => $faker->randomElement([
            'Enlace',
            'Turnador',
            'Abogado',
            'AbogadoTurnador',
            'Supervisor',
            'SuperAdministrador',
            ]),
        'email'          => $faker->email,
        'nombre'         => $faker->name,
        'apPaterno'      => $faker->firstName,
        'apMaterno'      => $faker->lastName,
        'remember_token' => str_random(10),
        'idInstitucion'  => '10',
        'idEstados'      => '9',
    ];
});
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'username'       => $faker->userName,
        'password'       => bcrypt('user'),
        'categoria'      => $faker->randomElement([
            'Enlace',
            'Turnador',
            'Abogado',
            'AbogadoTurnador',
            'Supervisor',
            'SuperAdministrador',
            'FunciónPublica',
            ]),
        'email'          => $faker->email,
        'nombre'         => $faker->name,
        'apPaterno'      => $faker->firstName,
        'apMaterno'      => $faker->lastName,
        'remember_token' => str_random(10),
        'idInstitucion'  => '10',
        'idEstados'      => '9',
    ];
});
