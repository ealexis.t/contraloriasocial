<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 45)->unique();
            $table->string('password', 255);
            $table->enum('categoria', [
                'Enlace',
                'Turnador',
                'Abogado',
                'AbogadoTurnador',
                'Supervisor',
                'SuperAdministrador',
            ]);
            $table->string('email', 80);
            $table->string('nombre', 50);
            $table->string('apPaterno', 50);
            $table->string('apMaterno', 50);
            $table->integer('idPrograma');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
