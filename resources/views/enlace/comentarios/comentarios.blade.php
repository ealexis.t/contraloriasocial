@extends('enlace.partials.layout-quejas')
@section('quejas')
<div class="titulos-denun">
    <ul class="nav nav-tabs">
        <div class="box-denun">
            <a href="{{URL::to('nuevas')}}">
                <li class="marco agregada-marco">
                    <p class="status-denun">Agregadas</p>
                </li>
            </a>
        </div>
        <div class="box-denun1">
            <a href="{{URL::to('aceptadas')}}">
                <li class="marco aceptada-marco">
                    <p class="status-denun">Aceptadas</p>
                </li>
            </a>
        </div>
        <div class="box-denun2">
            <a href="{{URL::to('concluidas')}}">
                <li class="marco concluida-marco">
                    <p class="status-denun">Concluidas</p>
                </li>
            </a>
        </div>
        <div class="box-denun3">
            <a href="{{URL::to('rechazadas')}}">
                <li class="marco rechazada-marco">
                    <p class="status-denun">Rechazadas</p>
                </li>
            </a>
        </div>
        <div class="margen box-denun4">
            <a href="{{URL::to('comentarios')}}">
                <li class="marco rechazada-marco-act">
                    <p class="status-denun">Comentarios</p>
                </li>
            </a>
        </div>
    </ul>
    <div class="titulo-denun">
        <p>Comentarios</p>
    </div>
</div>
<div class="tab-content quejas-content">
    <div class="tab-pane active">
        <table id="foo_2" class="table">
            <thead class="cabezara-tabla-denun">
                <th>Número</th>
                <th>última actualización</th>
                <th></th>
            </thead>
            <tbody class="tabla-denun">
                @foreach($comentarios as $comentario)
                    <tr>
                        <td class="num-noti"><a href="#" data-toggle="modal" data-target="#info{{$comentario->id}}">Comentario no. {{$comentario->id}}</a></td>
                        <td class="update-noti">{{$comentario->created_at}}</td>
                        <td class="mas-noti"><a href="#" class="pull-right" data-toggle="modal" data-target="#info{{$comentario->id}}">Ver más</a></td>
                    </tr>

                    <!--modal-->
                    <div class="modal fade" id="info{{$comentario->id}}" tabindex="-2" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header previ">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <img class ="logo-preview" src="{{ asset('/assets/img/preview/LOGO_chillon.png') }}" alt="Contraloria social">
                                <img class ="logo-sedesol" src="{{ asset('/assets/img/preview/LOGO_SEDESOL.png') }}" alt="SEDESOL">
                                </div>
                                <div class="side-titulo">
                                    <div class="barra-horizontal"></div>
                                    <h3 class="modal-title titulo-pre" id="myModalLabel">Comentario
                                    <br>
                                    No. {{$comentario->id}}</h3>
                                </div>
                                <div class="col-md-12 body-pre">
                                    <div class="preview well seccion">

                                    @foreach($catinstituciones as $institucion)
                                        @if($comentario->idInstitucion == $institucion->id)

                                        <div class="col-md-4 col-sm-4">
                                            <strong>Organismo sectorizado</strong>
                                            <input  class="form-control" placeholder="{{$institucion->nombreInstitucion}}" disabled>
                                        </div>

                                        @endif
                                    @endforeach

                                    @foreach($catprogramas as $programa)
                                        @if($comentario->idPrograma == $programa->id)

                                        <div class="col-md-12 col-sm-12">
                                            <strong>Programa central</strong>
                                            <input  class="form-control" placeholder="{{$programa->nombrePrograma}}" disabled>
                                        </div>

                                        @endif
                                    @endforeach

                                    <div class="col-md-12 col-sm-12">
                                        <strong>Comentario</strong>
                                        <br>
                                        <textarea class="form-control" disabled>{{$comentario->descripcionComentario}}</textarea>
                                    </div>

                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <a type="button" class="imprimir" href="#">Imprimir</a>
                                    <a type="button" class="cancel" data-dismiss="modal">Cerrar</a>
                                </div>

                            </div>
                        </div>
                    </div>
                @endforeach
            </tbody>
        </table>
        <div class="pull-right">
            <br>
            {{$comentarios->links()}}
        </div>
    </div>
</div>
@endsection
