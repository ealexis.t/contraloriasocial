@extends('enlace.partials.layout-quejas')
@section('quejas')
<div class="titulos-denun">
    <ul class="nav nav-tabs">
        <div class="box-denun">
            <a href="{{URL::to('nuevas')}}">
                <li class="marco agregada-marco">
                    <p class="status-denun">Agregadas</p>
                </li>
            </a>
        </div>
        <div class="margen box-denun1">
            <a href="{{URL::to('aceptadas')}}">
                <li class="marco aceptada-marco-act">
                    <p class="status-denun">Aceptadas</p>
                </li>
            </a>
        </div>
        <div class="box-denun2">
            <a href="{{URL::to('concluidas')}}">
                <li class="marco concluida-marco">
                    <p class="status-denun">Concluidas</p>
                </li>
            </a>
        </div>
        <div class="box-denun3">
            <a href="{{URL::to('rechazadas')}}">
                <li class="marco rechazada-marco">
                    <p class="status-denun">Rechazadas</p>
                </li>
            </a>
        </div>
        <div class="box-denun4">
            <a href="{{URL::to('comentarios')}}">
                <li class="marco rechazada-marco">
                    <p class="status-denun">Comentarios</p>
                </li>
            </a>
        </div>
    </ul>
    <div class="titulo-denun">
        <p>Quejas aceptadas</p>
    </div>
</div>
<div class="tab-content quejas-content">
    <div class="tab-pane active">
        <table id="foo_2" class="table">
            <thead class="cabezara-tabla-denun">
                <th>Número de queja</th>
                <th>última actualización</th>
                <th></th>
            </thead>
            <tbody class="tabla-denun">
                @foreach($quejas as $queja)
                    <tr>
                        <td class="num-noti"><a href="#" data-toggle="modal" data-target="#info{{$queja->id}}">La queja no. {{$queja->id}}</a></td>
                        <td class="update-noti">{{$queja->updated_at}}</td>
                        <td class="mas-noti"><a href="#" class="pull-right" data-toggle="modal" data-target="#info{{$queja->id}}">Ver más</a></td>
                    </tr>
                    <?php
                    /*
                    ******************************************************************************************
                    **                                  Modal Actualizar                                    **
                    ******************************************************************************************
                    */
                    ?>
                    <div class="modal fade" id="info{{$queja->id}}" tabindex="-2" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header previ">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <img class ="logo-preview" src="{{ asset('/assets/img/preview/LOGO_chillon.png') }}" alt="Contraloria social">
                                <img class ="logo-sedesol" src="{{ asset('/assets/img/preview/LOGO_SEDESOL.png') }}" alt="SEDESOL">
                                </div>
                                <div class="side-titulo">
                                    <div class="barra-horizontal"></div>
                                    <h3 class="modal-title titulo-pre" id="myModalLabel">Registro de queja o denuncia
                                    <br>
                                    No. {{$queja->id}}</h3>
                                </div>
                                <div class="col-md-12 body-pre">
                                    <div class="preview well seccion">

                                        <h4>Información del denunciante o quejoso</h4>
                                        @foreach($quejosos as $quejoso)
                                            @if($queja->idQuejoso == $quejoso->id)
                                                <div class="col-md-4 col-sm-4">
                                                    <strong>Nombre</strong>
                                                    <input class="form-control" placeholder="{{$quejoso->nombre}}" disabled>
                                                </div>

                                                <div class="col-md-4 col-sm-4">
                                                    <strong>Apellido paterno</strong>
                                                    <input  class="form-control" placeholder="{{$quejoso->apPaterno}}" disabled>
                                                </div>

                                                <div class="col-md-4 col-sm-4">
                                                    <strong>Apellido materno</strong>
                                                    <input  class="form-control" placeholder="{{$quejoso->apMaterno}}" disabled>
                                                </div>

                                                <div class="col-md-4 col-sm-4">
                                                    <strong>Calle</strong>
                                                    <input  class="form-control" placeholder="{{$quejoso->calle}}" disabled>
                                                </div>

                                                <div class="col-md-2 col-sm-2">
                                                    <strong>Núm. interior</strong>
                                                    <input  class="form-control" placeholder="{{$quejoso->numInt}}" disabled>
                                                </div>

                                                <div class="col-md-2 col-sm-2">
                                                    <strong>Núm. exterior</strong>
                                                    <input  class="form-control" placeholder="{{$quejoso->numExt}}" disabled>
                                                </div>

                                                <div class="col-md-4 col-sm-4">
                                                    <strong>Colonia</strong>
                                                    <input  class="form-control" placeholder="{{$quejoso->colonia}}" disabled>
                                                </div>

                                                <div class="col-md-2 col-sm-2">
                                                    <strong>C.P.</strong>
                                                    <input  class="form-control" placeholder="{{$quejoso->cp}}" disabled>
                                                </div>

                                                @foreach($estadosCat as $estadoCat)
                                                    @if($quejoso->idEstado == $estadoCat->id)

                                                        <div class="col-md-4 col-sm-4">
                                                            <strong>Estado</strong>
                                                            <input  class="form-control" placeholder="{{$estadoCat->nombreEstado}}" disabled>
                                                        </div>

                                                    @endif
                                                @endforeach

                                                @foreach($municipiosCat as $municipioCat)
                                                    @if(($quejoso->municipio*1) == $municipioCat->id)

                                                        <div class="col-md-4 col-sm-4">
                                                            <strong>Municipio</strong>
                                                            <input  class="form-control" placeholder="{{$municipioCat->nombreMunicipio}}" disabled>
                                                        </div>

                                                    @endif
                                                @endforeach

                                                <div class="col-md-4 col-sm-4">
                                                    <strong>Télefono</strong>
                                                    <input  class="form-control" placeholder="{{$quejoso->telefono}}" disabled>
                                                </div>

                                                <div class="col-md-4 col-sm-4">
                                                    <strong>Email</strong>
                                                    <input  class="form-control" placeholder="{{$quejoso->email}}" disabled>
                                                </div>

                                            @endif
                                        @endforeach

                                    </div>

                                    <div class="preview well seccion2">

                                        <h4>Lugar de los hechos</h4>
                                            <div class="col-md-4 col-sm-4">
                                                <strong>Calle</strong>
                                                <input  class="form-control" placeholder="{{$queja->calle}}" disabled>
                                            </div>

                                            <div class="col-md-2 col-sm-2">
                                                <strong>Núm. interior</strong>
                                                <input  class="form-control" placeholder="{{$queja->numInt}}" disabled>
                                            </div>

                                            <div class="col-md-2 col-sm-2">
                                                <strong>Núm. exterior</strong>
                                                <input  class="form-control" placeholder="{{$queja->numExt}}" disabled>
                                            </div>

                                            <div class="col-md-4 col-sm-4">
                                                <strong>Colonia</strong>
                                                <input  class="form-control" placeholder="{{$queja->colonia}}" disabled>
                                            </div>

                                            @foreach($catestados as $estado)
                                                @if($queja->idEstado == $estado->id)

                                                    <div class="col-md-4 col-sm-4">
                                                        <strong>Estado</strong>
                                                        <input  class="form-control" placeholder="{{$estado->nombreEstado}}" disabled>
                                                    </div>

                                                @endif
                                            @endforeach

                                            @foreach($catmunicipios as $municipio)
                                                @if(($queja->municipio*1) == $municipio->id)

                                                <div class="col-md-4 col-sm-4">
                                                    <strong>Municipio</strong>
                                                    <input  class="form-control" placeholder="{{$municipio->nombreMunicipio}}" disabled>
                                                </div>

                                                @endif
                                            @endforeach

                                            <div class="col-md-2 col-sm-2">
                                                <strong>Codigo postal</strong>
                                                <input  class="form-control" placeholder="{{$queja->cp}}" disabled>
                                            </div>

                                    </div>

                                    <div class="preview well seccion3">
                                        <h4>Fecha y hora de los hechos</h4>

                                        <div class="col-md-4 col-sm-4">
                                            <strong>Fecha de los hechos</strong>
                                            <input  class="form-control" placeholder="{{$queja->fechaHechos}}" disabled>
                                        </div>

                                        <div class="col-md-4 col-sm-4">
                                            <strong>Hora de los hechos:</strong>
                                            <input  class="form-control" placeholder="{{$queja->horaAproximada}}" disabled>
                                        </div>

                                    </div>

                                    <div class="preview well seccion4">

                                        <h4>Descripción de los hechos</h4>

                                        <div class="sub-horizontal"></div>
                                        <h4 class="sub">Nombre del funcionario</h4>

                                        <div class="col-md-4 col-sm-4">
                                            <strong>Nombre</strong>
                                            <input  class="form-control" placeholder="{{$queja->nombreFuncionario}}" disabled>
                                        </div>

                                        <div class="col-md-4 col-sm-4">
                                            <strong>Apellido parterno</strong>
                                            <input  class="form-control" placeholder="{{$queja->apellidoPatFuncionario}}" disabled>
                                        </div>

                                        <div class="col-md-4 col-sm-4">
                                            <strong>Apellido materno</strong>
                                            <input  class="form-control" placeholder="{{$queja->apellidoMatFuncionario}}" disabled>
                                        </div>

                                        <div class="col-md-4 col-sm-4">
                                            <strong>Cargo</strong>
                                            <input  class="form-control" placeholder="{{$queja->cargoFuncionario}}" disabled>
                                        </div>

                                        <div class="col-md-4 col-sm-4">
                                            <strong>Adscripción</strong>
                                            <input  class="form-control" placeholder="{{$queja->areaAdscripcion}}" disabled>
                                        </div>

                                        @foreach($cattipoquejas as $tipoQueja)
                                            @if($queja->idTipoQueja == $tipoQueja->id)

                                            <div class="col-md-4 col-sm-4">
                                                <strong>Tipo de queja</strong>
                                                <input  class="form-control" placeholder="{{$tipoQueja->nombreQueja}}" disabled>
                                            </div>

                                            @endif
                                        @endforeach

                                        @foreach($catinstituciones as $institucion)
                                            @if($queja->idInstitucion == $institucion->id)

                                            <div class="col-md-4 col-sm-4">
                                                <strong>Organismo centralizado</strong>
                                                <input  class="form-control" placeholder="{{$institucion->nombreInstitucion}}" disabled>
                                            </div>

                                            @endif
                                        @endforeach

                                        @foreach($catprogramas as $programa)
                                            @if($queja->idPrograma == $programa->id)

                                            <div class="col-md-8 col-sm-8">
                                                <strong>Programa central</strong>
                                                <input  class="form-control" placeholder="{{$programa->nombrePrograma}}" disabled>
                                            </div>

                                            @endif
                                        @endforeach

                                        <div class="hechos">
                                            <div class="sub-horizontal"></div>
                                            <h4 class="sub">Hechos</h4>

                                            <div class="col-md-12 col-sm-12">
                                                <strong>Descripción de pruebas</strong>
                                                <textarea class="form-control" placeholder="{{$queja->descripcionPruebas}}" disabled></textarea>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="preview well seccion5">
                                        <h4>Descripción de los hechos</h4>

                                        <strong>Descripción de los hechos</strong>
                                        <br>
                                        <textarea class="form-control textare-final" placeholder="" disabled>Los datos personales proporcionados por usted serán protegidos, en términos de los artículos 18, 20 y 21 de la Ley Federal de Transparencia y Acceso a la Información Pública Gubernamental; así como Sexto, Noveno, Décimo y Undécimo de los Lineamientos de Protección de Datos Personales, publicados en el Diario Oficial de la Federación el 30 de septiembre de 2005, disposiciones aplicables en términos del Tercero Transitorio del Decreto por el que se expide la Ley General de Transparencia y Acceso a la Información Pública, publicado en el Diario Oficial de la Federación el 04 de mayo del 2015.

{{$queja->descripcionPruebas}}

                                        </textarea>
                                    </div>

                                </div>

                                <div class="modal-footer">
                                    <a type="button" class="imprimir" href="#">Imprimir</a>
                                    <a type="button" class="cancel" data-dismiss="modal">Cerrar</a>
                                </div>

                            </div>
                        </div>
                    </div>
                @endforeach
            </tbody>
        </table>
        <div class="pull-right">
            <br>
            {{$quejas->links()}}
        </div>
    </div>
</div>
@endsection
