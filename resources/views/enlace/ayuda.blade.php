@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        @include('partials.menu-ayuda',array())
        <div class="col-md-9 juntar">
			<div class="portlet paddingless">
			    <div class="portlet-title line">
			        <div class="ayuda">
			            <h1 class="fontayuda">AYUDA</h1>
			        </div>
			        <div class="col-md-12 bodyhelp">

			        	<div class="body-contacto">
			        		<div class="barra-h"></div>
							<p class="centered heading-default primera">
				            	Estimado Usuario
				          	</p>
				          	<p class="contact">
				          		Para dudas o comentarios,<br>
				          		puede contactarnos vía correo electrónico
				          	<p class="contact">
				          		A <a href="mailto:correo@sedesol.gob.mx" class="font-red">correo@sedesol.gob.mx</a>
				          		<br>
				          		o vía télefonica al <span class="font-red">5328 5000</span> ext. <span class="font-red">51486</span>
				          	</p>
			          	</div>
						<div class="backhelp">
			        		<img style="width:450px;margin-left: 450px;" src="{{ asset('assets/img/icoenlace/ico/	slogan.png') }}">
			        	</div>
			        </div>
			    </div>
			    <div class="portlet-body">

			    </div>
			</div>
    	</div>
    </div>
</div>
@endsection
