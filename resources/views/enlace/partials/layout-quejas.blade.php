@extends('layouts.app')
@section('style-extra')
   {{Html::style('/assets/css/login.css')}}
@endsection
@section('content')
<div class="container">
    <div class="row">
        @include('partials.menu-quejas',array())
        <div class="col-md-9 denuncia">
            @if(Auth::user()->categoria == 'Enlace')
                <a href="#" class="pull-right new-queja notiqueja" data-target="#6" data-toggle="modal" style="font-size:18px; margin-top:-13px;">Comentario</a>
                <a href="#" class="pull-right new-queja notiqueja" data-target="#3" data-toggle="modal" style="font-size:18px; margin-top:-13px;">Nueva queja</a>
        	@endif
    		<div class="portlet paddingless">
			    <div class="portlet-title line cabezera-denun">
			        <div class="caption">
			            <h1 class="font-denun">Quejas y denuncias</h1>
			        </div>
			    </div>
			    <div class="portlet-body">
			        <!--BEGIN TABS-->
			        <div class="tabbable tabbable-custom">
			        	@yield('quejas')
			        </div>
			        <!--END TABS-->
			    </div>
			</div>
    	</div>
    </div>
</div>
@if(Auth::user()->categoria == 'Enlace')
    @include('partials.modals.comentario',array())
	@include('partials.modals.enlace',array())
@endif
@endsection
@section('script-extra')
  {{Html::script('/assets/js/validationform.js')}}

@endsection
