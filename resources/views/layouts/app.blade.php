<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Contraloria Social</title>
    <!-- Fonts -->
    <link rel="shortcut icon" href="/assets/img/favicon.ico">
    <!-- Fonts START -->
    <!--link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css"-->
    <!-- Fonts END -->
    <!-- Global styles START-->
    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>

    {{ Html::style('/assets/global/plugins/font-awesome/css/font-awesome.min.css') }}
    {{ Html::style('/assets/global/plugins/bootstrap/css/bootstrap.min.css') }}
    {{ Html::style('/assets/global/css/components.css') }}
    {{ Html::style('/assets/frontend/layout/css/style.css') }}
    {{ Html::style('/assets/frontend/layout/css/style-responsive.css') }}
    {{ Html::style('/assets/frontend/layout/css/themes/green.css') }}
    {{ Html::style('/assets/frontend/layout/css/custom.css') }}
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
    {{ Html::script('/assets/js/jquery.js') }}
    {{ Html::script('/assets/js/jquery.dataTables.js') }}
    {{ Html::style('/assets/css/jquery.dataTables.min.css') }}
    {{ Html::style('/assets/css/modals.css') }}
    {{ Html::style('/assets/css/style.css') }}
    {{Html::style('/assets/css/bootstrap-datepicker.min.css')}}
    {{Html::style('/assets/css/bootstrap-datepicker3.min.css')}}
    {{Html::style('/assets/css/jquery.ptTimeSelect.css')}}

    @yield('style-extra')
    <!-- Global styles END-->
</head>
<body id="app-layout">

        <div id="siPruebas" class="modal-success" style="display:none">
            <div class="message col-md-4 col-md-offset-4">
                <a class="pull-right" href="{{ URL('modal') }}">Cerrar</a>
                <div class="text-center">
                    <img class="icon-mensaje" src="{{ asset('/assets/img/home/mensaje.png') }}" alt="" height="100px" width="100px">
                </div>
                <hr style="border:solid #444 2px;">
                <p class="text-center">
                    ¡Queja guardada exitosamente!
                </p>
            </div>
        </div>

        <div id="noPruebas" class="modal-success" style="display:none">
            <div class="message col-md-4 col-md-offset-4">
                <a class="pull-right" href="{{ URL('modal') }}">Cerrar</a>
                <div class="text-center">
                    <img class="icon-mensaje" src="{{ asset('/assets/img/home/mensaje.png') }}" alt="" height="100px" width="100px">
                </div>
                <hr style="border:solid #444 2px;">
                <p class="text-center">
                    ¡Queja guardada exitosamente!
                </p>
                <p class="text-center">
                    Nota:No se añadieron pruebas.
                </p>
            </div>
        </div>


    @if(Session::has('message-comentario'))
        <div class="modal-success">
            <div class="message col-md-4 col-md-offset-4">
                <a class="pull-right" href="{{ URL('comentarios') }}">Cerrar</a>
                <div class="text-center">
                    <img class="icon-mensaje" src="{{ asset('/assets/img/home/mensaje.png') }}" alt="" height="100px" width="100px">
                </div>
                <hr style="border:solid #444 2px;">
                <p class="text-center">
                    {{Session::get('message-comentario')}}
                </p>
            </div>
        </div>
    @endif

    @if(Session::has('message-error'))
        <div class="modal-success">
            <div class="message col-md-4 col-md-offset-4">
                <a class="pull-right" href="{{ URL('modal') }}">Cerrar</a>
                <div class="text-center">
                    <img class="icon-mensaje" src="{{ asset('/assets/img/icons/icon_error.png') }}" alt="" height="100px" width="100px">
                </div>
                <hr style="border:solid #444 2px;">
                <p class="text-center">
                    {{ Session::get('message-error') }}
                </p>
            </div>
        </div>
        <script>
            function timer(){
                $(".modal-success").hide('fast');
                setInterval(redirect, 2000);
            }
            function redirect(){
                $("modal-success").css('display', 'none');
            }
            setInterval(timer, 2000);
        </script>
    @endif

    @if (Auth::guest())

    @else
    <nav class="" style="background:#4c484a; height:120px;">
        <div class="container">
            <div class="navbar-header">
                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{ asset('/assets/img/home/logo_frame.png') }}" width="200px" height="200px" style="z-index:10; position:relative; top:15px;left: 30px;">
                </a>
            </div>
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right login-menu">
                    <!-- Authentication Links -->
                    <li>
                        <a>Hola, {{ Auth::user()->nombre }}</a>
                    </li>
                    <li><a href="{{ URL('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Cerrar Sesion</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="col-md-3 col-md-offset-9">
    <br>
    <br>
    <br>
    <!--
        {{ Form::open(array('name' => 'f1','url' => 'busqueda',  'method' => 'post', 'class'=>''))}}

            <input class="searchTerm" type="search" name="queja" placeholder="Buscar">

        {{ Form::close() }}
        <br>
    -->
    </div>

    @endif
    @yield('content')
    <script type="text/javascript">
        $("#foo").dataTable({
                order: [[1, "desc"]]
            });
        $("#foo_2").dataTable({
                order: [[1, "desc"]]
            });
        $("#foo_3").dataTable({
                order: [[1, "desc"]]
            });
        $("#foo_4").dataTable({
                order: [[1, "desc"]]
            });
    </script>
    <!-- JavaScripts -->
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
    {{ Html::script('/assets/global/plugins/jquery-1.11.0.min.js') }}
    {{ Html::script('/assets/global/plugins/jquery-migrate-1.2.1.min.js') }}
    {{ Html::script('/assets/js/jquery-validation/dist/jquery.validate.min.js') }}
    {{ Html::script('/assets/global/plugins/bootstrap/js/bootstrap.min.js') }}
    {{ Html::script('/assets/frontend/layout/scripts/back-to-top.js') }}
    {{ Html::script('/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}
    {{ Html::script('/assets/frontend/layout/scripts/layout.js') }}
    <!-- END PAGE LEVEL JAVASCRIPTS -->
    {{ Html::script('/assets/global/plugins/jquery.blockui.min.js') }}
    {{ Html::script('/assets/global/scripts/metronic.js') }}
    {{Html::script('/assets/js/progressbar.js')}}
    {{Html::script('/assets/js/controlpicker.js')}}
    {{Html::script('/assets/js/bootstrap-datepicker.min.js')}}
    {{Html::script('assets/locales/bootstrap-datepicker.es.min.js') }}
    {{Html::script('/assets/js/jquery.ptTimeSelect.js')}}
    @yield('script-extra')
    <script>
        jQuery(document).ready(function() {
            // initiate layout and plugins
            Metronic.init(); // init metronic core components
            Layout.init(); // init current layout
        });
    </script>
</body>
</html>
