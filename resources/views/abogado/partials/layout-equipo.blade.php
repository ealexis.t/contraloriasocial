@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        @include('partials.menu-equipo',array())
        <div class="col-md-9">
        <h4><a href="#" class="pull-right new-miembro" data-toggle="modal" data-target="#nuevo-miembro">Agregar Miembro</a></h4>
    		<div class="portlet paddingless">
			    <div class="portlet-title line">
			        <div class="caption">
			            Equipo
			        </div>
			    </div>
			    <div class="portlet-body">
			        <!--BEGIN TABS-->
			        <div class="tabbable tabbable-custom">
			        	@yield('equipo')
			        </div>
			        <!--END TABS-->
			    </div>
			</div>
    	</div>
    </div>
</div>
<?php
/*
******************************************************************************************
**                             Modal Turnador Agregar Miembro                           **
******************************************************************************************
*/
?>
<div class="modal fade" id="nuevo-miembro" tabindex="-2" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Agrargar Miembro</h4>
            </div>
            <p>
                <h5 style="color:#666;" class="text-center">Formulario</h5 style="color:#666;">
                <br>
            </p>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary" data-dismiss="modal">Agregar</button>
            </div>
        </div>
    </div>
</div>
@endsection