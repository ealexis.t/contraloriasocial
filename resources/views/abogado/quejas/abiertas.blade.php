@extends('enlace.partials.layout-quejas')
@section('quejas')
<ul class="nav nav-tabs">
    <li class="">
        <a href="{{URL::to('nuevas')}}">
        Nuevas </a>
    </li>
    <li class="active">
        <a href="{{URL::to('abiertas')}}">
        Abiertas </a>
    </li>
    <li class="">
        <a href="{{URL::to('rechazadas')}}">
        Quejas Rechazadas </a>
    </li>
    <li class="">
        <a href="{{URL::to('concluidas')}}">
        Quejas Concluidas </a>
    </li>
    <li class="">
        <a href="{{URL::to('canceladas')}}">
        Quejas Canceladas </a>
    </li>
</ul>
<div class="tab-content">
    <div class="tab-pane active">
        <table id="foo_2" class="table">
            <thead>
                <tr>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($quejas as $queja)
                    <tr>
                        <td>
                        <a href="#" data-toggle="modal" data-target="#reasignar{{$queja->id}}">
                        La queja no. {{$queja->id}}
                        <br>
                        Asignada a:
                        @foreach($abogados as $abogado)
                            @if($abogado->id == $queja->idAbogado)
                                {{$abogado->nombre}} {{$abogado->apPaterno}} {{$abogado->apMaterno}}
                            @endif
                        @endforeach
                        </a>
                        </td>
                        <td>{{$queja->updated_at}}</td>
                        <td><a href="#" class="pull-right" data-toggle="modal" data-target="#reasignar{{$queja->id}}">Ver Detalles</a></td>
                    </tr>
                   <?php
                    /*
                    ******************************************************************************************
                    **                                  Modal Asignar                                   **
                    ******************************************************************************************
                    */
                    ?>
                    <div class="modal fade" id="reasignar{{$queja->id}}" tabindex="-2" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Queja no {{$queja->id}}</h4>
                                </div>
                                <div class="col-md-11 col-md-offset-1">
                                <br>
                                    <h4 style="color:#666;">Lugar:</h4>
                                    <strong>Dirección:</strong>
                                    {{$queja->calle}}, {{$queja->numInt}}, {{$queja->numExt}}, {{$queja->colonia}}, {{$queja->municipio}},
                                    <br>
                                    <strong>Codigo postal:</strong>
                                    {{$queja->cp}}
                                    <br>
                                    <strong>Fecha y hora:</strong>
                                    {{$queja->fechaHechos}} {{$queja->horaAproximada}}
                                    <hr>
                                    <h4>Funcionario</h4>
                                    <strong>Nombre del funcionario:</strong>
                                    {{$queja->nombreFuncionario}} {{$queja->apellidoPatFuncionario}} {{$queja->apellidoMatFuncionario}}
                                    <br>
                                    <strong>Cargo:</strong>
                                    {{$queja->cargoFuncionario}}
                                    <br>
                                    <strong>Area:</strong>
                                    {{$queja->areaAdscripcion}}
                                    <br>
                                    <strong>Dependencia:</strong>
                                    {{$queja->dependenciaGobierno}}
                                    <br>
                                    <hr>
                                    <strong>Narativa:</strong>
                                    <br>
                                    <p>{{$queja->narrativa}}</p>
                                    <br>
                                    <strong>Pruebas:</strong>
                                    <br>
                                    <p>{{$queja->descripcionPruebas}}</p>
                                    <hr>
                                {{ Form::open(array('name' => 'f1','url' => 'asignar/',  'method' => 'put', 'class'=>'form-horizontal row-fluid'))}}
                                    <div class="row col-md-12">
                                        <div class="col-md-3">
                                            <label class="">Asignar a:</label>
                                        </div>
                                        <div class="col-md-9">
                                            <select class="form-control" name="idAbogado" id="idAbogado{{$queja->id}}" onchange="boton({{$queja->id}})" required>
                                                <option value=""></option>
                                                @foreach($abogados as $abogado)
                                                <?php
                                                    $asignaciones = DB::table('queja')->where('estadoQueja', 'Aceptada')->where('idAbogado', $abogado->id)->count();
                                                 ?>
                                                <option value="{{$abogado->id}}" >{{$abogado->nombre}} {{$abogado->apPaterno}} {{$abogado->apMaterno}}:<span>Asignaciones:{{$asignaciones}}</span></option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-12">
                                            <input type="hidden" name="id" value="{{$queja->id}}">
                                            <hr>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default cerrar" data-dismiss="modal">Cerrar</button>
                                    <button type="button" class="btn btn-default asignar" data-dismiss="modal">Cancelar</button>
                                    <button type="submit" class="btn btn-primary asignar">Reasignar</button>
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                @endforeach
            </tbody>
        </table>
        {{$quejas->links()}}
    </div>
</div>
@section('script-extra')
<script src="/assets/js/app.js"></script>
@endsection
@endsection