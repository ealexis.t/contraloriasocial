@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        @include('partials.menu-bandeja',array())
        <div class="col-md-9">
			<div class="portlet paddingless">
			    <div class="portlet-title line">
			        <div class="caption">
			            <strong>Bandeja de Entrada</strong>
			        </div>
			    </div>
			    <div class="portlet-body">
			        <!--BEGIN TABS-->
			        <div class="tabbable tabbable-custom">
			            <div class="tab-content">
			                <div class="tab-pane active" id="tab_1_1">
			                    <table id="" class="table table-striped table-hover">
			                        <thead>
			                            <tr>
			                                <th></th>
			                                <th></th>
			                                <th></th>
			                            </tr>
			                        </thead>
			                        <tbody>
		                        	@foreach($quejas as $queja)
					                        <tr>
					                            <td><a href="#" data-toggle="modal" data-target="#info{{$queja->id}}">Nueva Queja</a></td>
					          					<td>{{$queja->updated_at}}</td>
					          					<td><a href="#" class="pull-right" data-toggle="modal" data-target="#info{{$queja->id}}"><img class="detalles" src="{{ asset('/assets/img/icons/icon_deta.png') }}" alt="" height="20px"></a></td>
					                        </tr>
			                        		<?php
						                    /*
						                    ******************************************************************************************
						                    **                                  Modal Asignar                                   **
						                    ******************************************************************************************
						                    */
						                    ?>
						                    <div class="modal fade" id="info{{$queja->id}}" tabindex="-2" role="dialog" aria-labelledby="myModalLabel">
						                        <div class="modal-dialog" role="document">
						                            <div class="modal-content">
						                                <div class="modal-header">
						                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						                                <h4 class="modal-title" id="myModalLabel">Queja no {{$queja->id}}</h4>
						                                </div>
						                                <div class="col-md-11 col-md-offset-1">
						                                <br>
						                                    <h4 style="color:#666;">Lugar:</h4>
						                                    <strong>Dirección:</strong>
						                                    {{$queja->calle}}, {{$queja->numInt}}, {{$queja->numExt}}, {{$queja->colonia}}, {{$queja->municipio}},
						                                    <br>
						                                    <strong>Codigo postal:</strong>
						                                    {{$queja->cp}}
						                                    <br>
						                                    <strong>Fecha y hora:</strong>
						                                    {{$queja->fechaHechos}} {{$queja->horaAproximada}}
						                                    <hr>
						                                    <h4>Funcionario</h4>
						                                    <strong>Nombre del funcionario:</strong>
						                                    {{$queja->nombreFuncionario}} {{$queja->apellidoPatFuncionario}} {{$queja->apellidoMatFuncionario}}
						                                    <br>
						                                    <strong>Cargo:</strong>
						                                    {{$queja->cargoFuncionario}}
						                                    <br>
						                                    <strong>Area:</strong>
						                                    {{$queja->areaAdscripcion}}
						                                    <br>
						                                    <strong>Dependencia:</strong>
						                                    {{$queja->idInstitucion}}
						                                    <br>
						                                    <hr>
						                                    <strong>Narativa:</strong>
						                                    <br>
						                                    <p>{{$queja->narrativa}}</p>
						                                    <br>
						                                    <strong>Pruebas:</strong>
						                                    <br>
						                                    <p>{{$queja->descripcionPruebas}}</p>
						                                    @foreach($pruebas as $prueba)
							                                    @if($queja->id == $prueba->idQueja)
							                                    {{Form::open(array('name' => 'descargar', 'url' => 'abogado/descarga', 'method' => 'post', 'enctype' => 'multipart/form-data'))}}

							                                    	<input type="hidden" name="idqueja" value="{{$queja->id}}">

							                                    	<input type="submit" name="archivoDown" value="Descargar archivo">
							                                    {{ Form::close() }}
							                                    @endif
						                                    @endforeach
						                                    <hr>
						                                {{ Form::open(array('name' => 'f1','url' => 'responder',  'method' => 'put', 'class'=>'form-horizontal row-fluid'))}}
						                                	<div class="row col-md-12">
					                                        <div class="col-md-3">
					                                            <label class="">Respoder:</label>
					                                        </div>
					                                        <div class="col-md-9">
					                                            <select class="form-control" name="estado" required>
					                                            	<option value=""></option>
					                                                <option value="Concluida">Concluir</option>
					                                                <option value="Rechazada">Rechazar</option>
					                                            </select>
					                                        </div>
					                                        <div class="col-md-12">
					                                            <input type="hidden" name="id" value="{{$queja->id}}">
					                                            <hr>
					                                        </div>
					                                    </div>
						                                </div>
						                                <div class="modal-footer">
						                                    <button type="button" class="btn btn-default " data-dismiss="modal">Cancelar</button>
						                                    <button type="submit" class="btn btn-primary " >Aceptar</button>
						                                </div>
						                                {{ Form::close() }}
						                            </div>
						                        </div>
						                    </div>
						                @endforeach
						            </tbody>
						        </table>
						        <div class="pull-right">
						        <br>
						        {{$quejas->links()}}
						        </div>
						    </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@section('script-extra')
<script src="/assets/js/app.js"></script>
@endsection
@endsection
