@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        @include('partials.menu-ayuda',array())
        <div class="col-md-9">
			<div class="portlet paddingless">
			    <div class="portlet-title line">
			        <div class="caption">
			            Ayuda
			        </div>
			    </div>
			    <div class="portlet-body">
			        <article>
						<h1 class="centered heading-default">
			            	Contacto
			          	</h1>
			          	<p>
			          		Estimado usuario si desea ponerse en contacto  para consultar dudas, aclaraciones o
			          		hacernos saber sus comentarios puede hacerlo:
			          	</p>
			          	<h3>Vía correo electrónico</h3>
			          	<p>
			          		A través de la dirección <a href="mailto:correo@sedesol.gob.mx">correo@sedesol.gob.mx</a>
			          	</a>
			          	</p>
			        </article>
			    </div>
			</div>
    	</div>
    </div>
</div>
@endsection