@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        @include('partials.menu-equipo',array())
        <div class="col-md-9 juntar">
        <a href="#" class="pull-right new-miembro notiqueja" data-toggle="modal" data-target="#nuevo-miembro" style="font-size:18px; margin-top:-13px;">Agregar Miembro</a>
    		<div class="portlet paddingless">
			    <div class="portlet-title line encabeza-equipo">
			        <div class="caption notifica">
                        <h1 class="fontayuda">Equipo de trabajo</h1>
                    </div>
			    </div>
			    <div class="portlet-body">
			        <!--BEGIN TABS-->
			        <div class="tabbable tabbable-custom">
			        	@yield('equipo')
			        </div>
			        <!--END TABS-->
			    </div>
			</div>
    	</div>
    </div>
</div>

<?php
/*
******************************************************************************************
**                             Modal Turnador Agregar Miembro                           **
******************************************************************************************
*/
?>
<div class="modal fade" id="nuevo-miembro" tabindex="-2" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Agregar Miembro</h4>
            </div>

            {{ Form::open(array('name' => 'f1','url' => 'create_abogado',  'method' => 'post', 'class'=>'form-horizontal row-fluid'))}}
                <div class="col-md-12">
                    <label for="nombre">Nombre(s)</label>
                    <input type="text" name="nombre" class="form-control" placeholder="Nombre" value="{{ old('nombre') }}">
                    <div class="bg-danger text-center" id="_autor">{{$errors->first('nombre')}}</div>
                    <br>

                    <label for="apPaterno">Apellido Paterno</label>
                    <input type="text" name="apPaterno" class="form-control" placeholder="Apellido paterno" value="{{ old('apPaterno') }}">
                    <div class="bg-danger text-center" id="_autor">{{$errors->first('apPaterno')}}</div>
                    <br>

                    <label for="apMaterno">Apellido Materno</label>
                    <input type="text" name="apMaterno" class="form-control" placeholder="Apellido materno" value="{{ old('apMaterno') }}">
                    <div class="bg-danger text-center" id="_autor">{{$errors->first('apMaterno')}}</div>
                    <br>

                    <label for="username">Username</label>
                    <input type="text" name="username" class="form-control" placeholder="Username" value="{{ old('username') }}">
                    <div class="bg-danger text-center" id="_autor">{{$errors->first('username')}}</div>
                    <br>

                    <label for="username">Password</label>
                    <input type="text" name="password" class="form-control" placeholder="Password" value="{{ old('password') }}">
                    <div class="bg-danger text-center" id="_autor">{{$errors->first('password')}}</div>
                    <br>

                    <label for="username">Correo</label>
                    <input type="text" name="email" class="form-control" placeholder="Correo" value="{{ old('email') }}">
                    <div class="bg-danger text-center" id="_autor">{{$errors->first('email')}}</div>

                    {{ Form::label('id_programaQueja', 'Programa o dependencia')}}
                    {{ Form::select('id_programaQueja', $programas, null, ['class' => 'form-control']) }}
                    <div class="bg-danger text-center" id="_autor">{{$errors->first('id_programaQueja')}}</div>
                    <hr>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default cerrar" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-primary " onclick="this.disabled=true;this.form.submit();">Aceptar</button>
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@endsection
