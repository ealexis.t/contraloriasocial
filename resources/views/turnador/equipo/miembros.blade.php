@extends('turnador.partials.layout-equipo')
@section('equipo')
<div class="titulos-denun">
    <ul class="nav nav-tabs">
        <div class="box-denun">
            <a href="{{URL::to('equipo/asignaciones')}}">
                <li class="marco asignacion-marco medida">
                    <p class="status-denun">ASIGNACIONES</p>
                </li>
            </a>
        </div>
        <div class="box-denun1 margen">
            <a href="{{URL::to('equipo/miembros')}}">
                <li class="marco miembro-marco-act">
                    <p class="status-denun">MIEMBROS</p>
                </li>
            </a>
        </div>
    </ul>
    <div class="titulo-denun">
        <p>Miembros</p>
    </div>
</div>
<div class="tab-content">
    <div class="tab-pane active">
        <table id="" class="table table-striped table-hover">
			<thead>
				<tr>
				    <th class="col-md-1"></th>
				    <th></th>
				    <th></th>
				    <th></th>
				</tr>
			</thead>
			<tbody>
				@foreach($abogados as $abogado)
				<tr>
					<td class="col-md-1">
						<img src="/assets/img/photos/avatar.png" alt="miembro">
					</td>
					<td>
						Nombre:{{$abogado->nombre}} {{$abogado->apPaterno}} {{$abogado->apMaterno}}
						<br>
						Puesto:{{$abogado->categoria}}
						<br>
						Asignaciones:
					</td>
				    <td class="text-center">
				    <br>
				    	<a href="#" class="pull-right col-md-1" data-toggle="modal" data-target="#editar-miembro{{$abogado->id}}">
				    		<img src="/assets/img/icons/edit.png" alt="">
				    	</a>
				    </td>
				    <td class="text-center">
				    <br>
						<a href="#" class="" data-toggle="modal" data-target="#eliminar-miembro{{$abogado->id}}">
							<img src="/assets/img/icons/delete.png" alt="">
						</a>
				    </td>
				</tr>
				@include('partials.modals.turnador',array())
				@endforeach
			</tbody>
		</table>
    </div>
</div>
@endsection
