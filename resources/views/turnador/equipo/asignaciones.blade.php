@extends('turnador.partials.layout-equipo')
@section('equipo')
<div class="titulos-denun">
    <ul class="nav nav-tabs">
        <div class="box-denun margen">
            <a href="{{URL::to('equipo/asignaciones')}}">
                <li class="marco asignacion-marco-act medida">
                    <p class="status-denun">ASIGNACIONES</p>
                </li>
            </a>
        </div>
        <div class="box-denun1">
            <a href="{{URL::to('equipo/miembros')}}">
                <li class="marco miembro-marco">
                    <p class="status-denun">MIEMBROS</p>
                </li>
            </a>
        </div>
    </ul>
    <div class="titulo-denun">
        <p>Asignaciones</p>
    </div>
</div>
<div class="tab-content">
    <div class="tab-pane active">
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
        	@foreach($abogados as $abogado)
			<div class="panel back-asigna">
				<div class="panel-heading" role="tab" id="headingOne">
			        <a class="nextdom" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$abogado->id}}" aria-expanded="true" aria-controls="collapseOne">
			            <h4 id="{{$abogado->id}}" class="panel-title abo-asig">
			            	Abogado: {{$abogado->nombre}} {{$abogado->apPaterno}} {{$abogado->apMaterno}}
			        	</h4>
			        </a>
				</div>
				<div id="collapse{{$abogado->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
				    <div class="panel-body">
				    @foreach($idAbogados as $idAbogado)
				    	@if($idAbogado->idUsers == $abogado->id)
					    	@foreach($quejas as $queja)
					    		@if($idAbogado->id == $queja->idAbogado)
					    			Queja:{{$queja->id}} {{$queja->fechaHechos}} {{$queja->horaAproximada}} <br>
					    		@endif
					    	@endforeach
				    	@endif
				    @endforeach
				    </div>
				</div>
			</div>
			@endforeach
    	</div>
	</div>
</div>
@endsection
