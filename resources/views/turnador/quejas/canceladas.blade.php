@extends('enlace.partials.layout-quejas')
@section('quejas')
<ul class="nav nav-tabs">
    <li class="">
        <a href="{{URL::to('nuevas')}}">
        Nuevas </a>
    </li>
    <li class="">
        <a href="{{URL::to('abiertas')}}">
        Abiertas </a>
    </li>
    <li class="">
        <a href="{{URL::to('rechazadas')}}">
        Quejas Rechazadas </a>
    </li>
    <li class="">
        <a href="{{URL::to('concluidas')}}">
        Quejas Concluidas </a>
    </li>
    <li class="active">
        <a href="{{URL::to('canceladas')}}">
        Quejas Canceladas </a>
    </li>
</ul>
<div class="tab-content">
    <div class="tab-pane active">
        <table id="" class="table table-striped table-hover">
            <thead>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($quejas as $queja)
                    <tr>
                        <td><a href="#" data-toggle="modal" data-target="#info{{$queja->id}}">La queja no. {{$queja->id}}</a></td>
                        <td>{{$queja->updated_at}}</td>
                        <td><a href="#" class="pull-right" data-toggle="modal" data-target="#info{{$queja->id}}"><img class="detalles" src="{{ asset('/assets/img/icons/icon_deta.png') }}" alt="" height="20px"></a></td>
                    </tr>
                    <?php
                    /*
                    ******************************************************************************************
                    **                                  Modal Actualizar                                    **
                    ******************************************************************************************
                    */
                    ?>
                    <div class="modal fade" id="info{{$queja->id}}" tabindex="-2" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Queja no {{$queja->id}}</h4>
                                </div>
                                <div class="col-md-12">
                                <br>
                                    <h4 style="color:#666;">Lugar:</h4>
                                    <strong>Dirección:</strong>
                                    {{$queja->calle}}, {{$queja->numInt}}, {{$queja->numExt}}, {{$queja->colonia}}, {{$queja->municipio}},
                                    <br>
                                    <strong>Codigo postal:</strong>
                                    {{$queja->cp}}
                                    <br>
                                    <strong>Fecha y hora:</strong>
                                    {{$queja->fechaHechos}} {{$queja->horaAproximada}}
                                    <hr>
                                    <h4>Funcionario</h4>
                                    <strong>Nombre del funcionario:</strong>
                                    {{$queja->nombreFuncionario}} {{$queja->apellidoPatFuncionario}} {{$queja->apellidoMatFuncionario}}
                                    <br>
                                    <strong>Cargo:</strong>
                                    {{$queja->cargoFuncionario}}
                                    <br>
                                    <strong>Area:</strong>
                                    {{$queja->areaAdscripcion}}
                                    <br>
                                    <strong>Dependencia:</strong>
                                    {{$queja->idInstitucion}}
                                    <br>
                                    <hr>
                                    <strong>Narativa:</strong>
                                    <br>
                                    <p>{{$queja->narrativa}}</p>
                                    <br>
                                    <strong>Pruebas:</strong>
                                    <br>
                                    <p>{{$queja->descripcionPruebas}}</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </tbody>
        </table>
        <div class="pull-right">
            <br>
            {{$quejas->links()}}
        </div>
    </div>
</div>
@endsection