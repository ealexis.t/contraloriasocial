@extends('enlace.partials.layout-quejas')
@section('quejas')
<div class="titulos-denun">
    <ul class="nav nav-tabs">
        <div class="box-denun margen">
            <a href="{{URL::to('nuevas')}}">
                <li class="marco agregada-marco-act">
                    <p class="status-denun">Agregadas</p>
                </li>
            </a>
        </div>
        <div class="box-denun1">
            <a href="{{URL::to('aceptadas')}}">
                <li class="marco aceptada-marco">
                    <p class="status-denun">Aceptadas</p>
                </li>
            </a>
        </div>
        <div class="box-denun2">
            <a href="{{URL::to('concluidas')}}">
                <li class="marco concluida-marco">
                    <p class="status-denun">Concluidas</p>
                </li>
            </a>
        </div>
        <div class="box-denun3">
            <a href="{{URL::to('rechazadas')}}">
                <li class="marco rechazada-marco">
                    <p class="status-denun">Rechazadas</p>
                </li>
            </a>
        </div>
    </ul>
    <div class="titulo-denun">
        <p>Quejas agregadas</p>
    </div>
</div>
<div class="tab-content">
    <div class="tab-pane active">
        <table id="" class="table table-striped">
            <thead class="cabezara-tabla-denun">
                <th>Número de queja</th>
                <th>última actualización</th>
                <th></th>
            </thead>
            <tbody class="tabla-denun">
                @foreach($quejas as $queja)
                    <tr>
                        <td><a href="#" data-toggle="modal" data-target="#asignar{{$queja->id}}">La queja no. {{$queja->id}}</a></td>
                        <td>{{$queja->updated_at}}</td>
                        <td><a href="#" class="pull-right" data-toggle="modal" data-target="#asignar{{$queja->id}}"><img class="detalles" src="{{ asset('/assets/img/icons/icon_deta.png') }}" alt="" height="20px"></a></td>
                    </tr>
                    <?php
                    /*
                    ******************************************************************************************
                    **                                  Modal Asignar                                   **
                    ******************************************************************************************
                    */
                    ?>
                     <div class="modal fade" id="info{{$queja->id}}" tabindex="-2" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header previ">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <img class ="logo-preview" src="{{ asset('/assets/img/preview/LOGO_chillon.png') }}" alt="Contraloria social">
                                <img class ="logo-sedesol" src="{{ asset('/assets/img/preview/LOGO_SEDESOL.png') }}" alt="SEDESOL">
                                </div>
                                <div class="side-titulo">
                                    <div class="barra-horizontal"></div>
                                    <h3 class="modal-title titulo-pre" id="myModalLabel">Registro de queja o denuncia
                                    <br>
                                    No. {{$queja->id}}</h3>
                                </div>
                                <div class="col-md-12 body-pre">
                                    <div class="well seccion">

                                        <h4>Información del denunciante o quejoso</h4>
                                        @foreach($quejosos as $quejoso)
                                            @if($queja->idQuejoso == $quejoso->id)
                                                <div class="col-md-4 col-sm-4">
                                                    <strong>Nombre</strong>
                                                    <input class="form-control" placeholder="{{$quejoso->nombre}}" disabled>
                                                </div>

                                                <div class="col-md-4 col-sm-4">
                                                    <strong>Apellido paterno</strong>
                                                    <input  class="form-control" placeholder="{{$quejoso->apPaterno}}" disabled>
                                                </div>

                                                <div class="col-md-4 col-sm-4">
                                                    <strong>Apellido materno</strong>
                                                    <input  class="form-control" placeholder="{{$quejoso->apMaterno}}" disabled>
                                                </div>

                                                <div class="col-md-4 col-sm-4">
                                                    <strong>Calle</strong>
                                                    <input  class="form-control" placeholder="{{$quejoso->calle}}" disabled>
                                                </div>

                                                <div class="col-md-2 col-sm-2">
                                                    <strong>Núm. interior</strong>
                                                    <input  class="form-control" placeholder="{{$quejoso->numInt}}" disabled>
                                                </div>

                                                <div class="col-md-2 col-sm-2">
                                                    <strong>Núm. exterior</strong>
                                                    <input  class="form-control" placeholder="{{$quejoso->numExt}}" disabled>
                                                </div>

                                                <div class="col-md-4 col-sm-4">
                                                    <strong>Colonia</strong>
                                                    <input  class="form-control" placeholder="{{$quejoso->colonia}}" disabled>
                                                </div>

                                                <div class="col-md-2 col-sm-2">
                                                    <strong>C.P.</strong>
                                                    <input  class="form-control" placeholder="{{$quejoso->cp}}" disabled>
                                                </div>

                                                @foreach($estadosCat as $estadoCat)
                                                    @if($quejoso->idEstado == $estadoCat->id)

                                                        <div class="col-md-4 col-sm-4">
                                                            <strong>Estado</strong>
                                                            <input  class="form-control" placeholder="{{$estadoCat->nombreEstado}}" disabled>
                                                        </div>

                                                    @endif
                                                @endforeach

                                                @foreach($municipiosCat as $municipioCat)
                                                    @if(($quejoso->municipio*1) == $municipioCat->id)

                                                        <div class="col-md-4 col-sm-4">
                                                            <strong>Municipio</strong>
                                                            <input  class="form-control" placeholder="{{$municipioCat->nombreMunicipio}}" disabled>
                                                        </div>

                                                    @endif
                                                @endforeach

                                                <div class="col-md-4 col-sm-4">
                                                    <strong>Télefono</strong>
                                                    <input  class="form-control" placeholder="{{$quejoso->telefono}}" disabled>
                                                </div>

                                                <div class="col-md-4 col-sm-4">
                                                    <strong>Email</strong>
                                                    <input  class="form-control" placeholder="{{$quejoso->email}}" disabled>
                                                </div>

                                            @endif
                                        @endforeach

                                    </div>

                                    <div class="well seccion2">

                                        <h4>Lugar de los hechos</h4>
                                            <div class="col-md-4 col-sm-4">
                                                <strong>Calle</strong>
                                                <input  class="form-control" placeholder="{{$queja->calle}}" disabled>
                                            </div>

                                            <div class="col-md-2 col-sm-2">
                                                <strong>Núm. interior</strong>
                                                <input  class="form-control" placeholder="{{$queja->numInt}}" disabled>
                                            </div>

                                            <div class="col-md-2 col-sm-2">
                                                <strong>Núm. exterior</strong>
                                                <input  class="form-control" placeholder="{{$queja->numExt}}" disabled>
                                            </div>

                                            <div class="col-md-4 col-sm-4">
                                                <strong>Colonia</strong>
                                                <input  class="form-control" placeholder="{{$queja->colonia}}" disabled>
                                            </div>

                                            @foreach($catestados as $estado)
                                                @if($queja->idEstado == $estado->id)

                                                    <div class="col-md-4 col-sm-4">
                                                        <strong>Estado</strong>
                                                        <input  class="form-control" placeholder="{{$estado->nombreEstado}}" disabled>
                                                    </div>

                                                @endif
                                            @endforeach

                                            @foreach($catmunicipios as $municipio)
                                                @if(($queja->municipio*1) == $municipio->id)

                                                <div class="col-md-4 col-sm-4">
                                                    <strong>Municipio</strong>
                                                    <input  class="form-control" placeholder="{{$municipio->nombreMunicipio}}" disabled>
                                                </div>

                                                @endif
                                            @endforeach

                                            <div class="col-md-2 col-sm-2">
                                                <strong>Codigo postal</strong>
                                                <input  class="form-control" placeholder="{{$queja->cp}}" disabled>
                                            </div>

                                    </div>

                                    <div class="well seccion3">
                                        <h4>Fecha y hora de los hechos</h4>

                                        <div class="col-md-4 col-sm-4">
                                            <strong>Fecha de los hechos</strong>
                                            <input  class="form-control" placeholder="{{$queja->fechaHechos}}" disabled>
                                        </div>

                                        <div class="col-md-4 col-sm-4">
                                            <strong>Hora de los hechos:</strong>
                                            <input  class="form-control" placeholder="{{$queja->horaAproximada}}" disabled>
                                        </div>

                                    </div>

                                    <div class="well seccion4">

                                        <h4>Descripción de los hechos</h4>

                                        <div class="sub-horizontal"></div>
                                        <h4 class="sub">Nombre del funcionario</h4>

                                        <div class="col-md-4 col-sm-4">
                                            <strong>Nombre</strong>
                                            <input  class="form-control" placeholder="{{$queja->nombreFuncionario}}" disabled>
                                        </div>

                                        <div class="col-md-4 col-sm-4">
                                            <strong>Apellido parterno</strong>
                                            <input  class="form-control" placeholder="{{$queja->apellidoPatFuncionario}}" disabled>
                                        </div>

                                        <div class="col-md-4 col-sm-4">
                                            <strong>Apellido materno</strong>
                                            <input  class="form-control" placeholder="{{$queja->apellidoMatFuncionario}}" disabled>
                                        </div>

                                        <div class="col-md-4 col-sm-4">
                                            <strong>Cargo</strong>
                                            <input  class="form-control" placeholder="{{$queja->cargoFuncionario}}" disabled>
                                        </div>

                                        <div class="col-md-4 col-sm-4">
                                            <strong>Adscripción</strong>
                                            <input  class="form-control" placeholder="{{$queja->areaAdscripcion}}" disabled>
                                        </div>

                                        @foreach($cattipoquejas as $tipoQueja)
                                            @if($queja->idTipoQueja == $tipoQueja->id)

                                            <div class="col-md-4 col-sm-4">
                                                <strong>Tipo de queja</strong>
                                                <input  class="form-control" placeholder="{{$tipoQueja->nombreQueja}}" disabled>
                                            </div>

                                            @endif
                                        @endforeach

                                        @foreach($catinstituciones as $institucion)
                                            @if($queja->idInstitucion == $institucion->id)

                                            <div class="col-md-4 col-sm-4">
                                                <strong>Dependecia</strong>
                                                <input  class="form-control" placeholder="{{$institucion->nombreInstitucion}}" disabled>
                                            </div>

                                            @endif
                                        @endforeach

                                        @foreach($catprogramas as $programa)
                                            @if($queja->idPrograma == $programa->id)

                                            <div class="col-md-8 col-sm-8">
                                                <strong>Dependecia</strong>
                                                <input  class="form-control" placeholder="{{$programa->nombrePrograma}}" disabled>
                                            </div>

                                            @endif
                                        @endforeach

                                        <div class="hechos">
                                            <div class="sub-horizontal"></div>
                                            <h4 class="sub">Hechos</h4>

                                            <div class="col-md-12 col-sm-12">
                                                <strong>Descripción de pruebas</strong>
                                                <textarea class="form-control" placeholder="{{$queja->descripcionPruebas}}" disabled></textarea>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="well seccion5">
                                        <h4>Descripción de los hechos</h4>

                                        <strong>Descripción de los hechos</strong>
                                        <br>
                                        <textarea class="form-control textare-final" placeholder="" disabled>Los datos personales proporcionados por usted serán protegidos, en términos de los artículos 18, 20 y 21 de la Ley Federal de Transparencia y Acceso a la Información Pública Gubernamental; así como Sexto, Noveno, Décimo y Undécimo de los Lineamientos de Protección de Datos Personales, publicados en el Diario Oficial de la Federación el 30 de septiembre de 2005, disposiciones aplicables en términos del Tercero Transitorio del Decreto por el que se expide la Ley General de Transparencia y Acceso a la Información Pública, publicado en el Diario Oficial de la Federación el 04 de mayo del 2015.

                                        {{$queja->descripcionPruebas}}

                                        </textarea>
                                    </div>

                                </div>

                               {{ Form::open(array('name' => 'f1','url' => 'asignar/',  'method' => 'put', 'class'=>'form-horizontal row-fluid'))}}
                                    <div class="row col-md-12">
                                        <div class="col-md-3">
                                            <label class="">Asignar a:</label>
                                        </div>
                                        <div class="col-md-9">
                                            <select class="form-control" name="idAbogado" id="idAbogado{{$queja->id}}" onchange="boton({{$queja->id}})" required>
                                                <option value=""></option>
                                                @foreach($abogados as $abogado)
                                                    @foreach($idAbogados as $idAbogado)
                                                        @if($abogado->id == $idAbogado->idUsers)
                                                            <?php
                                                                $asignaciones = DB::table('queja')->where('estadoQueja', 'Aceptada')->where('idAbogado', $idAbogado->id)->count();
                                                            ?>
                                                            <option value="{{$idAbogado->id}}" @if($queja->idAbogado == $idAbogado->id) selected @endif>{{$abogado->nombre}} {{$abogado->apPaterno}} {{$abogado->apMaterno}}:<span>Asignaciones:{{$asignaciones}}</span></option>
                                                        @endif
                                                    @endforeach
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-12">
                                            <input type="hidden" name="id" value="{{$queja->id}}">
                                            <hr>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default cerrar" data-dismiss="modal">Cerrar</button>
                                    <button type="button" class="btn btn-default asignar" data-dismiss="modal">Cerrar</button>
                                    <button type="submit" class="btn btn-primary asignar">Asignar</button>
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                @endforeach
            </tbody>
        </table>
        <div class="pull-right">
            <br>
            {{$quejas->links()}}
        </div>
    </div>
</div>
@section('script-extra')
<script src="/assets/js/app.js"></script>
@endsection
@endsection
