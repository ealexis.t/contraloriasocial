@extends('enlace.partials.layout-quejas')
@section('quejas')
<ul class="nav nav-tabs">
    <div class="box-denun">
        <a href="{{URL::to('nuevas')}}">
            <li class="marco agregada-marco-act">
                <p class="status-denun">Agregadas</p>
            </li>
        </a>
    </div>
    <div class="box-denun1">
        <a href="{{URL::to('aceptadas')}}">
            <li class="marco aceptada-marco">
                <p class="status-denun">Aceptadas</p>
            </li>
        </a>
    </div>
    <div class="box-denun2">
        <a href="{{URL::to('concluidas')}}">
            <li class="marco concluida-marco">
                <p class="status-denun">Concluidas</p>
            </li>
        </a>
    </div>
    <div class="box-denun3">
        <a href="{{URL::to('rechazadas')}}">
            <li class="marco rechazada-marco">
                <p class="status-denun">Rechazadas</p>
            </li>
        </a>
    </div>
</ul>
<div class="tab-content">
    <div class="tab-pane active">
        <table id="" class="table table-striped">
            <thead class="cabezara-tabla-denun">
                <th>Número de queja</th>
                <th>última actualización</th>
                <th></th>
            </thead>
            <tbody class="tabla-denun">
                @foreach($quejas as $queja)
                    <tr>
                        <td>
                        <a href="#" data-toggle="modal" data-target="#reasignar{{$queja->id}}">
                        La queja no. {{$queja->id}}
                        <br>
                        Asignada a:
                        @foreach($idAbogados as $idAbogadoAsignado)
                            @if($idAbogadoAsignado->id == $queja->idAbogado)
                                @foreach($abogados as $abogadoAsignado)
                                    @if($abogadoAsignado->id == $idAbogadoAsignado->idUsers)
                                        {{$abogadoAsignado->nombre}} {{$abogadoAsignado->apPaterno}} {{$abogadoAsignado->apMaterno}}
                                    @endif
                                @endforeach
                            @endif
                        @endforeach
                        </a>
                        </td>
                        <td>{{$queja->updated_at}}</td>
                        <td><a href="#" class="pull-right" data-toggle="modal" data-target="#reasignar{{$queja->id}}"><img class="detalles" src="{{ asset('/assets/img/icons/icon_deta.png') }}" alt="" height="20px"></a></td>
                    </tr>
                   <?php
                    /*
                    ******************************************************************************************
                    **                                  Modal Asignar                                   **
                    ******************************************************************************************
                    */
                    ?>
                    <div class="modal fade" id="reasignar{{$queja->id}}" tabindex="-2" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Queja no {{$queja->id}}</h4>
                                </div>
                                <div class="col-md-11 col-md-offset-1">
                                <br>
                                    <h4 style="color:#666;">Lugar:</h4>
                                    <strong>Dirección:</strong>
                                    {{$queja->calle}} {{$queja->numInt}} {{$queja->numExt}}, {{$queja->colonia}}, {{$queja->municipio}}
                                    <br>
                                    <strong>Codigo postal:</strong>
                                    {{$queja->cp}}
                                    <br>
                                    <strong>Fecha y hora:</strong>
                                    {{$queja->fechaHechos}} {{$queja->horaAproximada}}
                                    <hr>
                                    <h4>Funcionario</h4>
                                    <strong>Nombre del funcionario:</strong>
                                    {{$queja->nombreFuncionario}} {{$queja->apellidoPatFuncionario}} {{$queja->apellidoMatFuncionario}}
                                    <br>
                                    <strong>Cargo:</strong>
                                    {{$queja->cargoFuncionario}}
                                    <br>
                                    <strong>Area:</strong>
                                    {{$queja->areaAdscripcion}}
                                    <br>
                                    <strong>Dependencia:</strong>
                                    {{$queja->idInstitucion}}
                                    <br>
                                    <hr>
                                    <strong>Narativa:</strong>
                                    <br>
                                    <p>{{$queja->narrativa}}</p>
                                    <br>
                                    <strong>Pruebas:</strong>
                                    <br>
                                    <p>{{$queja->descripcionPruebas}}</p>
                                    <hr>
                                {{ Form::open(array('name' => 'f1','url' => 'asignar/',  'method' => 'put', 'class'=>'form-horizontal row-fluid'))}}
                                    <div class="row col-md-12">
                                        <div class="col-md-3">
                                            <label class="">Asignar a:</label>
                                        </div>
                                        <div class="col-md-9">
                                            <select class="form-control" name="idAbogado" id="idAbogado{{$queja->id}}" onchange="boton({{$queja->id}})" required>
                                                <option value=""></option>
                                                @foreach($abogados as $abogado)
                                                    @foreach($idAbogados as $idAbogado)
                                                        @if($abogado->id == $idAbogado->idUsers)
                                                            <?php
                                                                $asignaciones = DB::table('queja')->where('estadoQueja', 'Aceptada')->where('idAbogado', $idAbogado->id)->count();
                                                            ?>
                                                            <option value="{{$idAbogado->id}}" @if($queja->idAbogado == $idAbogado->id) selected @endif>{{$abogado->nombre}} {{$abogado->apPaterno}} {{$abogado->apMaterno}}:<span>Asignaciones:{{$asignaciones}}</span></option>
                                                        @endif
                                                    @endforeach
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-12">
                                            <input type="hidden" name="id" value="{{$queja->id}}">
                                            <hr>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default cerrar" data-dismiss="modal">Cerrar</button>
                                    <button type="button" class="btn btn-default asignar" data-dismiss="modal">Cancelar</button>
                                    <button type="submit" class="btn btn-primary asignar">Reasignar</button>
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                    </div>
                @endforeach
            </tbody>
        </table>
        <div class="pull-right">
        <br>
        {{$quejas->links()}}
        </div>
    </div>
</div>
@section('script-extra')
<script src="/assets/js/app.js"></script>
@endsection
@endsection
