@extends('enlace.partials.layout-quejas')
@section('quejas')
<div class="titulos-denun">
    <ul class="nav nav-tabs">
        <div class="box-denun">
            <a href="{{URL::to('nuevas')}}">
                <li class="marco agregada-marco">
                    <p class="status-denun">Agregadas</p>
                </li>
            </a>
        </div>
        <div class="box-denun1">
            <a href="{{URL::to('aceptadas')}}">
                <li class="marco aceptada-marco">
                    <p class="status-denun">Aceptadas</p>
                </li>
            </a>
        </div>
        <div class="box-denun2">
            <a href="{{URL::to('concluidas')}}">
                <li class="marco concluida-marco">
                    <p class="status-denun">Concluidas</p>
                </li>
            </a>
        </div>
        <div class="box-denun3 margen">
            <a href="{{URL::to('rechazadas')}}">
                <li class="marco rechazada-marco-act">
                    <p class="status-denun">Rechazadas</p>
                </li>
            </a>
        </div>
    </ul>
    <div class="titulo-denun">
        <p>Quejas rechazadas</p>
    </div>
</div>
<div class="tab-content">
    <div class="tab-pane active">
        <table id="" class="table table-striped">
            <thead class="cabezara-tabla-denun">
                <th>Número de queja</th>
                <th>última actualización</th>
                <th></th>
            </thead>
            <tbody class="tabla-denun">
                @foreach($quejas as $queja)
                    <tr>
                        <td><a href="#" data-toggle="modal" data-target="#info{{$queja->id}}">La queja no. {{$queja->id}}</a></td>
                        <td>{{$queja->updated_at}}</td>
                        <td><a href="#" class="pull-right" data-toggle="modal" data-target="#info{{$queja->id}}"><img class="detalles" src="{{ asset('/assets/img/icons/icon_deta.png') }}" alt="" height="20px"></a></td>
                    </tr>
                    <?php
                    /*
                    ******************************************************************************************
                    **                                  Modal Actualizar                                    **
                    ******************************************************************************************
                    */
                    ?>
                    <div class="modal fade" id="info{{$queja->id}}" tabindex="-2" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Queja no {{$queja->id}}</h4>
                                </div>
                                <div class="col-md-12">
                                <br>
                                    <h4 style="color:#666;">Lugar:</h4>
                                    <strong>Dirección:</strong>
                                    {{$queja->calle}}, {{$queja->numInt}}, {{$queja->numExt}}, {{$queja->colonia}}, {{$queja->municipio}},
                                    <br>
                                    <strong>Codigo postal:</strong>
                                    {{$queja->cp}}
                                    <br>
                                    <strong>Fecha y hora:</strong>
                                    {{$queja->fechaHechos}} {{$queja->horaAproximada}}
                                    <hr>
                                    <h4>Funcionario</h4>
                                    <strong>Nombre del funcionario:</strong>
                                    {{$queja->nombreFuncionario}} {{$queja->apellidoPatFuncionario}} {{$queja->apellidoMatFuncionario}}
                                    <br>
                                    <strong>Cargo:</strong>
                                    {{$queja->cargoFuncionario}}
                                    <br>
                                    <strong>Area:</strong>
                                    {{$queja->areaAdscripcion}}
                                    <br>
                                    <strong>Dependencia:</strong>
                                    {{$queja->idInstitucion}}
                                    <br>
                                    <hr>
                                    <strong>Narativa:</strong>
                                    <br>
                                    <p>{{$queja->narrativa}}</p>
                                    <br>
                                    <strong>Pruebas:</strong>
                                    <br>
                                    <p>{{$queja->descripcionPruebas}}</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                    <button type="submit" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </tbody>
        </table>
        <div class="pull-right">
            <br>
            {{$quejas->links()}}
        </div>
    </div>
</div>
@endsection
