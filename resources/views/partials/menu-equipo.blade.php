<div class="col-md-3 partial-back">
    <ul class="nav nav-tabs nav-pills nav-stacked menu-lateral" role="tablist">
        <li role="presentation"><a class="noactivate" href="{{URL::action('BandejaController@index')}}" ><strong>Actividad reciente</strong></a></li>
        @if(Auth::user()->categoria != 'Abogado')
        <li role="presentation"><a class="icons1" href="{{URL::action('QuejasController@index')}}"><strong>Quejas y denuncias</strong></a></li>
        @endif
        @if(Auth::user()->categoria == 'SuperAdministrador' || Auth::user()->categoria == 'Turnador')
        <li role="presentation"><a class="icons3act" href="{{URL::to('equipo')}}"><strong>Equipo</strong></a></li>
        @endif
        <li role="presentation"><a class="icons2"href="{{URL::to('ayuda')}}"><strong>Ayuda</strong></a></li>
    </ul>
    @if(Auth::user()->categoria == 'Enlace')
        <img class="partial-logo" src="{{ asset('/assets/img/icoenlace/ico/logosedesolblanco.png') }}" alt="sedesol">
    @endif
    @if(Auth::user()->categoria == 'Turnador')
        <img class="partial-logo-turnador" src="{{ asset('/assets/img/icoenlace/ico/logosedesolblanco.png') }}" alt="sedesol">
    @endif
</div>
