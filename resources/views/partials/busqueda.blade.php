@extends('enlace.partials.layout-quejas')
@section('quejas')
<ul class="nav nav-tabs">
</ul>
<br>
@if($queja)
    <div class="col-md-11 col-md-offset-1">
        @foreach($queja as $queja)
            <h3 style="color:#666;">Queja no. {{$queja->id}} {{$queja->estadoQueja}}</h3>
            <h4 style="color:#666;">Lugar:</h4>
            <strong>Dirección:</strong>
            {{$queja->calle}} {{$queja->numInt}} {{$queja->numExt}} {{$queja->colonia}} {{$queja->municipio}}
            <br>
            <strong>Codigo postal:</strong>
            {{$queja->cp}}
            <br>
            <strong>Fecha y hora:</strong>
            {{$queja->fechaHechos}} {{$queja->horaAproximada}}
            <hr>
            <h4>Funcionario</h4>
            <strong>Nombre del funcionario:</strong>
            {{$queja->nombreFuncionario}} {{$queja->apellidoPatFuncionario}} {{$queja->apellidoMatFuncionario}}
            <br>
            <strong>Cargo:</strong>
            {{$queja->cargoFuncionario}}
            <br>
            <strong>Area:</strong>
            {{$queja->areaAdscripcion}}
            <br>
            <strong>Dependencia:</strong>
            {{$queja->dependenciaGobierno}}
            <br>
            <hr>
            <strong>Narativa:</strong>
            <br>
            <p>{{$queja->narrativa}}</p>
            <br>
            <strong>Pruebas:</strong>
            <br>
            <p>{{$queja->descripcionPruebas}}</p>
            <hr>
            <h4>Abogado Asignado</h4>
            @foreach($abogados as $asignado)
                @if($queja->idAbogado == $asignado->id)
                <p><strong>Nombe:</strong>{{$asignado->nombre}} {{$asignado->apPaterno}} {{$asignado->apMaterno}}</p>
                @endif
            @endforeach
            <hr>
            @if($queja->estadoQueja == 'Pendiente' || $queja->estadoQueja == 'Aceptada')
                {{ Form::open(array('name' => 'f1','url' => 'asignar-queja',  'method' => 'put', 'class'=>'form-horizontal row-fluid'))}}
                <div class="row col-md-12">
                    <div class="col-md-3">
                        <label class="">Asignar a:</label>
                    </div>
                    <div class="col-md-9">
                        <select class="form-control" name="idAbogado" id="idAbogado{{$queja->id}}" onchange="boton({{$queja->id}})" required>
                            <option value=""></option>
                            @foreach($abogados as $abogado)
                            <?php
                                $asignaciones = DB::table('queja')->where('estadoQueja', 'Aceptada')->where('idAbogado', $abogado->id)->count();
                             ?>
                            <option value="{{$abogado->id}}" @if($queja->idAbogado == $abogado->id) selected @endif>{{$abogado->nombre}} {{$abogado->apPaterno}} {{$abogado->apMaterno}}:<span>Asignaciones:{{$asignaciones}}</span></option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-12">
                        <input type="hidden" name="id" value="{{$queja->id}}">
                        <hr>
                    </div>
                    @if(Auth::user()->categoria == 'Turnador' || Auth::user()->categoria == 'SuperAdministrador')
                        <div style="padding:70px;">
                            <button type="submit" class="pull-right btn btn-primary asignar">Reasignar</button>
                            <a href="{{URL::to('bandeja')}}" class="pull-right btn btn-default asignar" data-dismiss="modal">Cancelar</a>
                            <a href="{{URL::to('bandeja')}}" class="pull-right btn btn-default cerrar">Cerrar</a>
                        </div>
                    @endif
                </div>
                {{ Form::close() }}
            @else
                <a href="{{URL::to('bandeja')}}" class="pull-right btn btn-default cerrar">Cerrar</a>
            @endif
        @endforeach
    </div>
@else
<h4 class="text-center">No se encontraron resultados.</h4>
@endif
@endsection
@section('script-extra')
<script src="/assets/js/app.js"></script>
@endsection