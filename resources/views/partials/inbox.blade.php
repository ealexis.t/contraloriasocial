<!-- Tables Inbox -->
<div class="col-md-9">
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="entrada">
            @include('partials.components.bandeja',array())
        </div>
    </div>
</div>
<!-- End Tables Inbox -->
<!-- Modals -->
@include('partials.modals.enlace',array())
@include('partials.modals.comentario',array())
<!-- End Modals -->

