<!-- Modal -->
<div class="modal modal-comentario fade" id="6" tabindex="-6" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="header-titulo">
        <h1 class="modal-title" id="myModalLabel">Comentario</h1>
      </div>
      <div class="modal-header">
        <h3 class="motivo-comentario">Comentario para la Función Pública</h3>
      </div>
      <div class="modal-body">
        {!! Form::open(array('id' => 'comentario', 'url' => 'enlace/comentario_crear', 'method' => 'POST')) !!}

          <meta name="csrf-token" content="{{ csrf_token() }}">
          <fieldset id="comentar" class="well num">

            <div class="form-group col-md-6 col-sm-6">
                {{ Form::label('id_institucion', '*Organismo sectorizado')}}
                {{ Form::select('id_institucion', [''=>'Elegir Institución']+$instituciones, 0, ['id' => 'instutucion', 'class' => 'form-control']) }}
                <p class="error"></p>
            </div>

            <div class="form-group col-md-6 col-sm-6">
                {{ Form::label('id_programaQueja', '*Programas centrales')}}
                {{ Form::select('id_programaQueja',[''=>'Elegir Programa'], null,['id'=>'id_programaQueja', 'class'=>'form-control']) }}
                <p class="error"></p>
            </div>

            <div class="form-group col-md-12 col-sm-12">
              <label for="descripcionComentario">*Comentario</label>
              <textarea name="descripcionComentario" class="form-control"></textarea>
              <p class="error"></p>
            </div>

          </fieldset>
          <div class="botonera">
              <a class="cancel" id="btn-cancel" data-dismiss="modal">Cancelar</a>

              {{ Form::submit('Finalizar', ['id' => 'mandar-comentario', 'class' => 'botons']) }}
              <!--Evita el shot onClick="this.disabled=true, this.form.submit()"-->
          </div>
        {!! Form::close() !!}
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
