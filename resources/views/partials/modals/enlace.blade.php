
<?php
/*
******************************************************************************************
**                                  Modal Actualizar                                    **
******************************************************************************************
*/
?>
<div class="modal fade" id="2" tabindex="-2" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Queja no 1122</h4>
            </div>
            <p>
                <h5 style="color:#666;" class="text-center">Subir Prueba</h5 style="color:#666;">
                <br>
                <input type="file" class="btn btn-sm" value="" />

            </p>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary" data-dismiss="modal">Actualizar y Enviar</button>
            </div>
        </div>
    </div>
</div>

<?php
/*
******************************************************************************************
**                               Modal Nueva Queja                                      **
******************************************************************************************
*/
?>
<div class="modal miModal fade bs-modal-lg" id="3" tabindex="-3" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="header-titulo">
                    <h1 class="modal-title" id="myModalLabel">Nueva Queja</h1>
            </div>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="probar">
                    <div class="cont">
                        <p class="textos-pasos">Información del denunciante</p>
                        <p class="textos-pasos">Lugar de los hechos</p>
                        <p class="textos-pasos">Fecha y hora de los hechos</p>
                        <p class="textos-pasos">Descripción de los hechos</p>
                        <div class="progress">
                            <div id="progress-bar" class="progress-bar" role="progressbar" style="width: 0%;">
                            </div>
                            <span class="myspan first border-change"></span>
                            <span class="myspan second"></span>
                            <span class="myspan third"></span>
                            <span class="myspan fourth"></span>
                        </div>

                    </div>
                </div>
            </div>
                <div class="modal-body">
                        <form id="form1">
                            <meta name="csrf-token" content="{{ csrf_token() }}">
                                <h2 class="motivo">Información del denunciante o quejoso</h2>
                                <fieldset id="paso1" class="well num">
                                    <img class="numeros-pasos" src="{{ asset('assets/img/numeros/numero1.png') }}">
                                <div class="ocultar-mostrar">

                                                <div class="form-group col-md-4 col-sm-4">
                                                    <label for="nombre">Nombre(s)</label>
                                                    <input type="text" name="nombre" class="form-control" id="nombre" placeholder="Nombre" value="{{ old('nombre') }}">
                                                </div>
                                                <div class="form-group col-md-4 col-sm-4">
                                                    <label for="apPaterno">Apellido Paterno</label>
                                                    <input type="text" name="apPaterno" class="form-control" id="apPaterno" placeholder="Apellido paterno" value="{{ old('apPaterno') }}">
                                                </div>
                                                <div class="form-group col-md-4 col-sm-4">
                                                    <label for="apMaterno">Apellido Materno</label>
                                                    <input type="text" name="apMaterno" class="form-control" id="apMaterno" placeholder="Apellido materno" value="{{ old('apMaterno') }}">
                                                </div>

                                                <div class="form-group col-md-5 col-sm-5">
                                                    <label for="calleQuejoso">Calle</label>
                                                    <input type="text" name="calle" class="form-control" id="calleQuejoso" placeholder="Calle o Avenida" value="{{ old('calle') }}">
                                                </div>

                                                <div class="form-group col-md-3 col-sm-3">
                                                    <label for="numExtQuejoso">Núm. exterior</label>
                                                    <input type="text" name="numExt" class="form-control" id="numExtQuejoso" placeholder="Núm. ext." value="{{ old('numExt') }}">
                                                </div>
                                                <div class="form-group col-md-3 col-sm-3">
                                                    <label for="numIntQuejoso">Núm. interior</label>
                                                    <input type="text" name="numInt" class="form-control" id="numIntQuejoso" placeholder="Núm int." value="{{ old('numInt') }}">
                                                </div>

                                                <div class="form-group col-md-5 col-sm-5">
                                                    <label for="coloniaQuejoso">Colonia</label>
                                                    <input type="text" name="colonia" class="form-control" id="coloniaQuejoso" placeholder="Colonia" value="{{ old('colonia') }}">
                                                </div>

                                                <div class="form-group col-md-3 col-sm-3">
                                                    <label for="cpQuejoso">C.P.</label>
                                                    <input type="text" name="cp" class="form-control" id="cpQuejoso" placeholder="C.P." value="{{ old('cp') }}">
                                                </div>


                                                <div class="form-group col-md-6 col-sm-6">
                                                {{ Form::label('id_estado', 'Entidad Federativa')}}
                                                {{ Form::select('id_estado', [ 0 =>'Elegir Estado']+$estados, 0 , ['id'=>'estado','class'=>'form-control']) }}
                                                </div>
                                                <div class="form-group col-md-6 col-sm-6">
                                                {{ Form::label('id_municipio', 'Delegación o municipio')}}
                                                {{ Form::select('id_municipio', ['placeholder'=>'Elegir Delegación o municipio'], null,['id'=>'municipio', 'class'=>'form-control']) }}
                                                </div>

                                                <div class="form-group col-md-4 col-sm-4">
                                                    <label for="telefono">Teléfono</label>
                                                    <input type="tel" name="telefono" class="form-control" id="telefono" placeholder="Teléfono" value="{{ old('telefono') }}">
                                                    <p class="error"></p>
                                                </div>
                                                <div class="form-group col-md-4 col-sm-4">
                                                    <label for="email">Correo Electrónico</label>
                                                    <input type="email" name="email" class="form-control" id="email" placeholder="Correo electrónico" value="{{ old('correo') }}">
                                                </div>
                                            </div>
                                                <!--Quejoso ocultar-->
                                                <div class="form-group col-md-12 col-sm-12">
                                                    <div class="checkbox">
                                                        <label for="anonimo">
                                                            <input class="checkboxAnonimo" type="checkbox" name="anonimo"> Deseo hacer una denuncia Anónima
                                                        </label>
                                                    </div>
                                                </div>
                                </fieldset>
                                <div class="botonera">
                                    <a href="#" id="pass1" class="botons style-sig">Siguiente</a>
                                </div>
                            </form>

                            <form id="form2">
                                <!--Queja-->
                                <fieldset id="paso2" class="well num">
                                        <img class="numeros-pasos" src="{{ asset('assets/img/numeros/numero2.png') }}">
                                        <h3 class="col-md-12 col-sm-12">Lugar de los hechos</h3>
                                        <div class="form-group col-md-5 col-sm-5">
                                            <label for="calleQueja">*Calle</label>
                                            <input type="text" name="calleQueja" class="form-control" id="calleQueja" placeholder="Calle o Avenida" value="{{ old('calleQueja') }}">
                                        </div>

                                        <div class="form-group col-md-3 col-sm-3">
                                            <label for="numExtQueja">*Núm. exterior</label>
                                            <input type="text" name="numExtQueja" class="form-control" id="numExtQueja" placeholder="Núm. ext." value="{{ old('numExtQueja') }}">
                                        </div>
                                         <div class="form-group col-md-3 col-sm-3">
                                            <label for="numIntQueja">Núm. interior</label>
                                            <input type="text" name="numIntQueja" class="form-control" id="numIntQueja" placeholder="Núm. int." value="{{ old('numIntQueja') }}" />
                                        </div>

                                        <div class="form-group col-md-5 col-sm-5">
                                            <label for="coloniaQueja">*Colonia</label>
                                            <input type="text" name="coloniaQueja" class="form-control" id="coloniaQueja" placeholder="Colonia" value="{{ old('coloniaQueja') }}">
                                        </div>

                                        <div class="form-group col-md-3 col-sm-3">
                                            <label for="cpQueja">*C.P.</label>
                                            <input type="text" name="cpQueja" class="form-control" id="cpQueja" placeholder="C.P." value="{{ old('cpQueja') }}">
                                        </div>

                                        <div class="form-group col-md-6 col-sm-6">
                                            {{ Form::label('id_estadoQueja', '*Entidad Federativa')}}
                                            {{ Form::select('id_estadoQueja', [ '' =>'Elegir Estado']+$estados, 0, ['id'=>'estadoQueja','class'=>'form-control']) }}
                                        </div>
                                        <div class="form-group col-md-6 col-sm-6">
                                            {{ Form::label('id_municipioQueja', '*Delegación o municipio')}}
                                            {{ Form::select('id_municipioQueja',[''=>'Elegir Delegación o municipio'], null,['id'=>'municipioQueja', 'class'=>'form-control']) }}
                                        </div>
                                </fieldset>
                                <div class="botonera">
                                    <a href="#" id="atras2" class="botons style-ant">Anterior</a>
                                    <a href="#" id="pass2" class="botons style-sig">Siguiente</a>
                                </div>
                            </form>

                            <form id="form3">
                                <!--Segunda parte del formulario-->

                                <fieldset id="paso3" class="well num">
                                        <img class="numeros-pasos" src="{{ asset('assets/img/numeros/numero3.png') }}">
                                        <h3 class="col-md-12 col-sm-12">Fecha y hora de los hechos</h3>
                                        <div class="form-group col-md-4 col-sm-4">
                                            <label for="horaAproximada">*Fecha</label>
                                            <div class="input-group date">
                                                <input type="text" id="dp1" class="datepicker form-control" placeholder="Fecha de los hechos" name="date" value="{{ old('date') }}">
                                            </div>
                                        </div>
                                        <div class="form-group col-md-4 col-sm-4">
                                            <label for="horaAproximada">Hora Aproximada</label>
                                             <div class='input-group date' id='datetimepicker3'>
                                                <input type='text' class="form-control" placeholder="Hora de los hechos" id="fausto" name="time" value="{{ old('time') }}"/>
                                            </div>
                                        </div>
                                </fieldset>
                                <div class="botonera">
                                    <a href="#" id="atras3" class="botons style-ant">Anterior</a>
                                    <a href="#" id="pass3" class="botons style-sig">Siguiente</a>
                                </div>
                                <!--Tercera parte del formulario-->
                            </form>

                            <form id="form4" enctype="multipart/form-data">
                                <fieldset id="paso4" class="well num">
                                        <img class="numeros-pasos" src="{{ asset('assets/img/numeros/numero4.png') }}">
                                        <h3 class="col-md-12 col-sm-12">Descripción de los hechos</h3>
                                        <div class="form-group col-md-4 col-sm-4">
                                            <label for="nombreFuncionario">*Nombre(s) del funcionario</label>
                                            <input type="text" name="nombreFuncionario" class="form-control" id="nombreFuncionario" placeholder="Nombre del funcionario" value="{{ old('nombreFuncionario') }}">
                                        </div>
                                        <div class="form-group col-md-4 col-sm-4">
                                            <label for="apellidoPatFuncionario">*Apellido Paterno</label>
                                            <input type="text" name="apellidoPatFuncionario" class="form-control" id="apellidoPatFuncionario" placeholder="Apellido paterno" value="{{ old('apellidoPatFuncionario') }}">
                                        </div>
                                        <div class="form-group col-md-4 col-sm-4">
                                            <label for="apellidoMatFuncionario">*Apellido Materno</label>
                                            <input type="text" name="apellidoMatFuncionario" class="form-control" id="apellidoMatFuncionario" placeholder="Apellido materno" value="{{ old('apellidoMatFuncionario') }}">
                                        </div>

                                        <div class="form-group col-md-4 col-sm-4">
                                            <label for="cargoFuncionario">*Cargo del funcionario</label>
                                            <input type="text" name="cargoFuncionario" class="form-control" id="cargoFuncionario" placeholder="Cargo o puesto institucional" value="{{ old('cargoFuncionario') }}">
                                        </div>
                                        <div class="form-group col-md-4 col-sm-4">
                                            <label for="adscripcion">*Adscripción</label>
                                            <input type="text" name="adscripcion" class="form-control" id="adscripcion" placeholder="Adscripción" value="{{ old('adscripcion') }}">
                                        </div>
                                        <div class="form-group col-md-4 col-sm-4">
                                            {{ Form::label('id_tiposQueja', '*Tipo de queja')}}
                                            {{ Form::select('id_tiposQueja', $tiposQuejas, null, ['placeholder'=>'Elegir el tipo de queja' ,'class' => 'form-control']) }}
                                        </div>

                                        <div class="form-group col-md-5 col-sm-5">
                                            {{ Form::label('id_institucion', '*Organismo sectorizado')}}
                                            {{ Form::select('id_institucion', [''=>'Elegir Institución']+$instituciones, 0, ['id' => 'instutucionQueja', 'class' => 'form-control']) }}
                                        </div>


                                        <div class="form-group col-md-5 col-sm-5">
                                            {{ Form::label('id_programaQueja', '*Programas centrales')}}
                                            {{ Form::select('id_programaQueja',[''=>'Elegir Programa'], null,['id'=>'idProgramaQueja', 'class'=>'form-control']) }}
                                        </div>

                                        <div class="form-group col-md-6 col-sm-6">
                                            <label for="narrativa">*Hechos</label>
                                            <textarea name="narrativa" class="form-control"></textarea>
                                        </div>
                                        <div class="form-group col-md-6 col-sm-6">
                                            <label for="descripcion">Descripción de pruebas</label>
                                            <textarea name="descripcion" class="form-control"></textarea>
                                        </div>

                                        <div class="form-group col-md-12 col-sm-12">
                                            <label for="archivo">Subir archivo</label>
                                            <input type="file" name="archivo" id="arch">
                                        </div>
                                </fieldset>
                                <div class="botonera">
                                    <a class="cancel" id="btn-cancel" data-dismiss="modal">Cancelar</a>
                                    <a href="#" id="atras4" class="botons style-ant">Anterior</a>
                                    <!--Cuarta parte del formulario-->
                                    <a id="mandar" type="submit" class="botons">Finalizar</a>
                                    <!--Evita el shot onClick="this.disabled=true, this.form.submit()"-->
                                </div>

                            </form>



                </div>
    </div>
</div>
