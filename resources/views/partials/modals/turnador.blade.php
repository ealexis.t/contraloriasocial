<?php
/*
******************************************************************************************
**                             Modal Turnador Editar Miembro                            **
******************************************************************************************
*/
?>
<div class="modal fade" id="editar-miembro{{$abogado->id}}" tabindex="-2" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Editar Miembro</h4>
            </div>
            {{ Form::open(array('name' => 'f1','url' => 'update_abogado/',  'method' => 'put', 'class'=>'form-horizontal row-fluid'))}}
            <div class="form-body">
                <div class="row col-md-12">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nombre">Nombre(s)</label>
                            <input type="text" class="form-control" name="nombre" value="{{$abogado->nombre}}">
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="apPaterno">Apellido Paterno</label>
                            <input type="text" class="form-control" name="apPaterno" value="{{$abogado->apPaterno}}">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="apMaterno">Apellido Materno</label>
                            <input type="text" class="form-control" name="apMaterno" value="{{$abogado->apMaterno}}">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="id" value="{{$abogado->id}}">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary">Actualizar</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>

<?php
/*
******************************************************************************************
**                            Modal Turnador Eliminar Miembro                           **
******************************************************************************************
*/
?>
<div class="modal fade" id="eliminar-miembro{{$abogado->id}}" tabindex="-2" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Eliminar Miembro</h4>
            </div>
            {{ Form::open(array('name' => 'f1','url' => 'delete_abogado/',  'method' => 'put', 'class'=>'form-horizontal row-fluid'))}}
            <div class="form-body">
            <p>
                <h5 style="color:#666;" class="text-center">Seguro que desea eliminar a:</h5 style="color:#666;">
                <h5 style="color:#333;" class="text-center">{{$abogado->nombre}} {{$abogado->apPaterno}} {{$abogado->apMaterno}}</h5 style="color:#666;">
                <br>
            </p>
            <div class="modal-footer">
                <input type="hidden" name="id" value="{{$abogado->id}}">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary">Aceptar</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>