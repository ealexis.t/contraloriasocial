$(document).ready(function(){

  $.fn.serializeObjetc = function(){
      var o = {};
      var a = this.serializeArray();
      $.each(a, function(){
        if(o[this.name]){

          if(!o[this.name].push){
            o[this.name] = [o[this.name]];
          }
          o[this.name].push(this.value || '');

        }else{
          o[this.name] = this.value || '';
        }

      });
      return o;
    }

  //reset a los inputs al llamar una nueva queja
  $('.new-queja').click(function() {

    $('#paso3').hide();
    $('#pass3').hide();
    $('#atras3').hide();

    $('#paso4').hide();
    $('#atras4').hide();
    $('#mandar').hide();

    ahora = $('#paso2');
    despues = $('#paso1');
    ahora.hide();
    despues.show();

    ahoraSig = $('#pass2');
    despuesSig = $('#pass1');
    ahoraSig.hide();
    despuesSig.show();

    ahoraAtr = $('#atras2');
    ahoraAtr.hide();

    $('#form1').trigger('reset');
    $('#form2').trigger('reset');
    $('#form3').trigger('reset');
    $('#form4').trigger('reset');
    $('#comentario').trigger('reset');
  });

  //reset a los inputs en el boton cancelar
  $('#btn-cancel').click(function() {
    $('#form1').trigger('reset');
    $('#form2').trigger('reset');
    $('#form3').trigger('reset');
    $('#form4').trigger('reset');
    $('#comentario').trigger('reset');
  });


  //Validar si van los campos vacios para su llenado
  $('#pass1').click(function() {

    if($('input:text[name="nombre"]').val().length < 1){
      $('input:text[name="nombre"]').val('Vacio');
    }

    if($('input:text[name="apPaterno"]').val().length < 1){
      $('input:text[name="apPaterno"]').val('Vacio');
    }

    if($('input:text[name="apMaterno"]').val().length < 1){
      $('input:text[name="apMaterno"]').val('Vacio');
    }

    if($('input:text[name="calle"]').val().length < 1){
      $('input:text[name="calle"]').val('Vacio');
    }

    if($('input:text[name="numInt"]').val().length < 1){
      $('input:text[name="numInt"]').val('Vacio');
    }

    if($('input:text[name="numExt"]').val().length < 1){
      $('input:text[name="numExt"]').val('Vacio');
    }

    if($('input:text[name="colonia"]').val().length < 1){
      $('input:text[name="colonia"]').val('Vacio');
    }

    if($('input:text[name="cp"]').val().length < 1){
      $('input:text[name="cp"]').val('00000');
    }

    if($('select[name="id_estado"]').val() < 1){
      $('select[name="id_estado"]').val(34);
    }

    if( ($('select[name="id_municipio"]').val() == 'placeholder') || ($('select[name="id_municipio"]').val() == 'Elegir Delegación o municipio')){
      $('select[name="id_municipio"]').append('<option value="-1" selected="selected">Vacio</option>');
    }

    if($('#telefono').val() < 1){
      $('#telefono').val('0000000000');
    }

    if($('#email').val() < 1){
      $('#email').val('vacio@vacio');
    }

  });

  var myForm1;
  var myForm2;
  var myForm3;
  var myForm4;

  //Next al paso 2
  $('#pass1').click(function(){

    var form = $('#form1');
    form.validate({
      rules:{
        telefono:{digits: true, maxlength: 15,},
        cp:{digits: true, maxlength: 5, minlength: 5,},
      },
      messages:{
        cp:{
            digits: 'Deben de ser solo digitos',
            maxlength: 'Maximo de caracteres es de 5',
            minlength: 'Minimo de caracteres es de 5',
        },
        telefono:{
            digits: 'Deben de ser solo digitos',
            maxlength: 'Maximo de caracteres es de 15',
        },
      }
    });

    if(form.valid() == true){

      $('.second').addClass('border-change');
      $('.progress-bar').addClass('progress-bar1');
      $('.motivo').replaceWith('<h2 class="motivo">Queja o denuncia</h2>');

      ahora = $('#paso1');
      despues = $('#paso2');
      ahora.hide();
      despues.show();

      ahoraSig = $('#pass1');
      despuesSig = $('#pass2');
      ahoraSig.hide();
      despuesSig.show();

      ahoraAtr = $('#atras2');
      ahoraAtr.show();

      myForm1 = $('#form1').serializeObjetc();
    }

  });

  //atras al paso 1
  $('#atras2').click(function(){

    $('.second').removeClass('border-change');
    $('.progress-bar').removeClass('progress-bar1');
    $('.motivo').replaceWith('<h2 class="motivo">Información del denunciante o quejoso</h2>');

    ahora = $('#paso2');
    despues = $('#paso1');
    ahora.hide();
    despues.show();

    ahoraSig = $('#pass2');
    despuesSig = $('#pass1');
    ahoraSig.hide();
    despuesSig.show();

    ahoraAtr = $('#atras2');
    ahoraAtr.hide();
  });


  //Next al paso 3
  $('#pass2').click(function(){

    var form = $('#form2');
      form.validate({
        rules:{
            id_estadoQueja:{required: true,},
            id_municipioQueja:{required: true,},
            calleQueja:{required: true,},
            coloniaQueja:{required: true,},
            numIntQueja:{maxlength: 5,},
            numExtQueja:{required: true, maxlength: 5,},
            coloniaQueja:{required: true,},
            cpQueja:{required: true, digits: true, maxlength: 5, minlength: 5,},
        },
        messages:{
          id_municipioQueja:{
              required: 'Campo requerido',
          },

          id_estadoQueja:{
              required: 'Campo requerido',
          },

          calleQueja:{
              required: 'Campo requerido',
          },

          numIntQueja:{
              maxlength: 'Maximo de caracteres es de 5',
          },

          numExtQueja:{
              required: 'Campo requerido',
              maxlength: 'Maximo de caracteres es de 5',
          },

          coloniaQueja:{
              required: 'Campo requerido',
          },

          cpQueja:{
              required: 'Campo requerido',
              digits: 'Deben de ser solo digitos',
              maxlength: 'Maximo de caracteres es de 5',
              minlength: 'Minimo de caracteres es de 5',
          },
        }
      });

        if(form.valid() == true){
        $('.third').addClass('border-change');
        $('.progress-bar').addClass('progress-bar2');
        $('.motivo').replaceWith('<h2 class="motivo">Queja o denuncia</h2>');

        ahora = $('#paso2');
        despues = $('#paso3');
        ahora.hide();
        despues.show();

        ahoraSig = $('#pass2');
        despuesSig = $('#pass3');
        ahoraSig.hide();
        despuesSig.show();

        ahoraAtr = $('#atras2');
        despuesAtr = $('#atras3');
        ahoraAtr.hide();
        despuesAtr.show();

        myForm2 = $('#form2').serializeObjetc();

      }

  });

  //atras al paso 2
  $('#atras3').click(function(){

    $('.third').removeClass('border-change');
    $('.progress-bar').removeClass('progress-bar2');
    $('.motivo').replaceWith('<h2 class="motivo">Queja o denuncia</h2>');

    ahora = $('#paso3');
    despues = $('#paso2');
    ahora.hide();
    despues.show();

    ahoraSig = $('#pass3');
    despuesSig = $('#pass2');
    ahoraSig.hide();
    despuesSig.show();

    ahoraAtr = $('#atras3');
    despuesAtr = $('#atras2');
    ahoraAtr.hide();
    despuesAtr.show();

  });

  //Next al paso 4
  $('#pass3').click(function(){
  var form = $('#form3');
    form.validate({
      rules:{
        date:{required: true, date:true,},
        //time:{required: true,},
      },
      messages:{
        date:{
            required: 'Campo requerido',
            date: 'Debe colocar una fecha valida',
        },
        //time:{
        //    required: 'Campo requerido',
        //},
      }
    });

    if(form.valid() == true){
      $('.fourth').addClass('border-change');
      $('.progress-bar').addClass('progress-bar3');
      $('.motivo').replaceWith('<h2 class="motivo">Queja o denuncia</h2>');

      ahora = $('#paso3');
      despues = $('#paso4');
      ahora.hide();
      despues.show();

      ahoraSig = $('#pass3');
      despuesSig = $('#pass4');
      ahoraSig.hide();
      despuesSig.show();

      ahoraAtr = $('#atras3');
      despuesAtr = $('#atras4');
      ahoraAtr.hide();
      despuesAtr.show();

      enviar = $('#mandar');
      enviar.show();

      myForm3 = $('#form3').serializeObjetc();

    }

  });

  //atras al paso 3
  $('#atras4').click(function(){

    $('.fourth').removeClass('border-change');
    $('.progress-bar').removeClass('progress-bar3');
    $('.motivo').replaceWith('<h2 class="motivo">Queja o denuncia</h2>');

    ahora = $('#paso4');
    despues = $('#paso3');
    ahora.hide();
    despues.show();

    despuesSig = $('#pass3');
    despuesSig.show();

    ahoraAtr = $('#atras4');
    despuesAtr = $('#atras3');
    ahoraAtr.hide();
    despuesAtr.show();

    enviar = $('#mandar');
    enviar.hide();

  });

  $('#mandar').click(function(){
    var form = $('#form4');
      form.validate({
        rules:{
          id_tiposQueja:{required: true,},
          id_institucion:{required: true,},
          id_programaQueja:{required: true,},
          nombreFuncionario:{required: true,},
          apellidoPatFuncionario:{required: true,},
          apellidoMatFuncionario:{required: true,},
          cargoFuncionario:{required: true,},
          adscripcion:{required: true,},
          narrativa:{required: true,},
        },
        messages:{
          id_programaQueja:{
              required: 'Campo requerido',
          },

          id_tiposQueja:{
              required: 'Campo requerido',
          },

          id_institucion:{
              required: 'Campo requerido',
          },

          nombreFuncionario:{
              required: 'Campo requerido',
          },

          apellidoPatFuncionario:{
              required: 'Campo requerido',
          },

          apellidoMatFuncionario:{
              required: 'Campo requerido',
          },

          cargoFuncionario:{
              required: 'Campo requerido',
          },

          adscripcion:{
              required: 'Campo requerido',
          },

          narrativa:{
              required: 'Campo requerido',
          },
        }
      });

      if(form.valid() == true){

        var emptyFile = 'no';

        if($('input:file[name="archivo"]').val().length < 1){
          emptyFile = 'si';
        }
        //console.log(emptyFile);

        myForm4 = $('#form4').serializeObjetc();

        var file = $('#form4').find('input[name=archivo]').prop('files')[0];
        //console.log(file);
        var formData = new FormData();

        formData.append('saludos','Fausto');
        formData.append('archivo', file);
        //console.log(formData);

        jsonForm1 = myForm1;
        jsonForm2 = myForm2;
        jsonForm3 = myForm3;
        jsonForm4 = myForm4;

        var jsonFormFull = [];

        jsonFormFull.push(jsonForm1);
        jsonFormFull.push(jsonForm2);
        jsonFormFull.push(jsonForm3);
        jsonFormFull.push(jsonForm4);

        jsonFormFull = JSON.stringify(jsonFormFull);
        formData.append("datos",jsonFormFull);
        formData.append("archivoVacio", emptyFile);
        console.log(formData);

        $.ajax({
          url: 'enlace/queja/arch',
          type: 'POST',
          contentType: false,
          processData: false,
          data: formData,
        })
        .done(function(response) {

          if(response == 'ok'){
            $(".miModal").modal("toggle");
            $("#siPruebas").css('display', 'block');
          }

          if(response == 'sin pruebas'){
            $(".miModal").modal("toggle");
            $("#noPruebas").css('display', 'block');
          }

        })
        .fail(function() {
          console.log("error");
        })
        .always(function() {
          console.log("complete");
        });

      }
  });

});// complete click


$('#mandar-comentario').click(function(){
    var form = $('#comentario');
    form.validate({
      rules:{
        id_institucion:{required: true,},
        id_programaQueja:{required: true,},
        descripcionComentario:{required: true,},
      },
      messages:{
        id_institucion:{
            required: 'Campo requerido',
        },
        id_programaQueja:{
            required: 'Campo requerido',
        },
        descripcionComentario:{
            required: 'Campo requerido',
        },
      }
    });
});
