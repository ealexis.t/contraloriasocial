$(document).ready(function(){

    $('#').on('click', function(){

        $('#multiballot-form').validate({

            highlight: function(element, errorClass) {
                $(element).fadeOut(function() {
                    $(element).addClass('input-error');
                    $(element).fadeIn();
                });
            },

            unhighlight: function(element, errorClass) {
                $(element).fadeOut(function() {
                    $(element).removeClass('input-error');
                    $(element).fadeIn();
                });
            },


            rules:{
                tel:{digits: true, maxlength: 15,},

                cp:{digits: true, maxlength: 5,},

                calleQueja:{required: true,},

                numIntQueja:{required: true, maxlength: 5,},

                numExtQueja:{required: true, maxlength: 5,},

                coloniaQueja:{required: true,},

                cpQueja:{required: true, digits: true, maxlength: 5,},

                date:{required: true, date:true,},

                time:{required: true,},

                nombreFuncionario:{required: true,},

                apellidoPatFuncionario:{required: true,},

                apellidoMatFuncionario:{required: true,},

                cargoFuncionario:{required: true,},

                adscripcion:{required: true,},

                narrativa:{required: true,},

            },

            messages:{
                cp:{
                    digits: 'Deben de ser solo digitos',
                    maxlength: 'Maximo de caracteres es de 5',
                },

                tel:{
                    digits: 'Deben de ser solo digitos',
                    maxlength: 'Maximo de caracteres es de 15',
                },

                //Queja parte 1
                calleQueja:{
                    required: 'Campo requerido',
                },

                numIntQueja:{
                    required: 'Campo requerido',
                    maxlength: 'Maximo de caracteres es de 5',
                },

                numExtQueja:{
                    required: 'Campo requerido',
                    maxlength: 'Maximo de caracteres es de 5',
                },

                coloniaQueja:{
                    required: 'Campo requerido',
                },

                cpQueja:{
                    required: 'Campo requerido',
                    digits: 'Deben de ser solo digitos',
                    maxlength: 'Maximo de caracteres es de 5',
                },

                date:{
                    required: 'Campo requerido',
                    date: 'Debe colocar una fecha valida',
                },

                time:{
                    required: 'Campo requerido',
                },

                nombreFuncionario:{
                    required: 'Campo requerido',
                },

                apellidoPatFuncionario:{
                    required: 'Campo requerido',
                },

                apellidoMatFuncionario:{
                    required: 'Campo requerido',
                },

                cargoFuncionario:{
                    required: 'Campo requerido',
                },

                adscripcion:{
                    required: 'Campo requerido',
                },

                narrativa:{
                    required: 'Campo requerido',
                },

            }
        });
    });


});
