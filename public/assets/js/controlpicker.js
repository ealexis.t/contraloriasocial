$(document).ready(function(event) {

  console.log('Affirmative, Dave. I read you.');

  //Token de formulario
  $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
  });

  //collapse abogados
  $('.back-asigna').on('click',function(event) {
    var getId = event.target.id;
    if(!($('.nextdom').hasClass('collapsed'))){
      $('.back-asigna').addClass('back-asigna-arriba');
    }
    console.log(getId);
  });

  //evitar el doble scroll
  $('.new-queja').on('click', function(){
    $('#app-layout').addClass('ocultar-scroll');
  });

  $('a[data-toggle="modal"]').on('click', function(){
    $('#app-layout').addClass('ocultar-scroll');
  });


  //Esconder estado anonimo
  $("#estadoQueja option[value=" + 33 + "]").hide();
  $("#estadoQueja option[value=" + 34 + "]").hide();

  $("#estado option[value=" + 33 + "]").hide();
  $("#estado option[value=" + 34 + "]").hide();

  //Ajax estados y municipios dependientes
  $("#estado").change(function(event){
    $.get("quejas/municipios/"+event.target.value+"", function(response, estados){

      $("#municipio").empty();
      $("#municipio").append("<option value='Elegir Delegación o municipio'>Elegir Delegación o municipio</option>");
      for(i=0;i<response.length;i++){
        $("#municipio").append("<option value='"+response[i].id+"'> "+response[i].nombreMunicipio+"</option>");
      }

    });
  });

  $("#estadoQueja").change(function(event){
    $.get("quejas/municipios/"+event.target.value+"", function(response, estados){

      $("#municipioQueja").empty();
      $("#municipioQueja").append("<option value='Elegir Delegación o municipio'>Elegir Delegación o municipio</option>");
      for(i=0;i<response.length;i++){
        $("#municipioQueja").append("<option value='"+response[i].id+"'> "+response[i].nombreMunicipio+"</option>");
      }

    });
  });

  //Ajax instituciones y programas dependientes
  $("#instutucion").change(function(event){
    $.get("quejas/programas/"+event.target.value+"", function(response, estados){

      $("#id_programaQueja").empty();
      //$("#id_programaQueja").append("<option value='Elegir Delegación o municipio'>Elegir Delegación o municipio</option>");
      for(i=0;i<response.length;i++){
        $("#id_programaQueja").append("<option value='"+response[i].id+"'> "+response[i].nombrePrograma+"</option>");
      }

    });
  });

  //Ajax instituciones y programas dependientes
  $("#instutucionQueja").change(function(event){
    $.get("quejas/programas/"+event.target.value+"", function(response, estados){

      $("#idProgramaQueja").empty();
      //$("#idProgramaQueja").append("<option value='Elegir Delegación o municipio'>Elegir Delegación o municipio</option>");
      for(i=0;i<response.length;i++){
        $("#idProgramaQueja").append("<option value='"+response[i].id+"'> "+response[i].nombrePrograma+"</option>");
      }

    });
  });

  //DatePicker
  $(function(){
      $('.datepicker').datepicker({
          format: 'yyyy-mm-dd',
          language: 'es',
          todayHighlight: true,
          autoclose: true,
          endDate: '+0d',
      });
  });

  //TimePicker
  $(function () {
        $("input[name='time']").ptTimeSelect();
  });

  //Checkbox anonimo
  $(function (){
    $(".checkboxAnonimo").on( 'change', function() {
        if( $(this).is(':checked') ) {
            // Hacer algo si el checkbox ha sido seleccionado
            //$('fieldset').attr('disabled', 'true');
            $('.ocultar-mostrar').hide('slow');

            $('input:text[name="nombre"]').val('anonimo');
            $('input:text[name="apPaterno"]').val('anonimo');
            $('input:text[name="apMaterno"]').val('anonimo');
            $('input:text[name="calle"]').val('anonimo');
            $('input:text[name="numInt"]').val('S/N');
            $('input:text[name="numExt"]').val('S/N');
            $('input:text[name="colonia"]').val('anonimo');
            $('input:text[name="cp"]').val('S/CP');
            $('select[name="id_estado"]').val(33);
            $('select[name="id_municipio"]').append('<option value="0" selected="selected">Anonimo</option>');
            $('#telefono').val('anonimo');
            $('#email').val('anonimo@anonimo');

            //alert("El checkbox con valor " + $(this).val() + " ha sido seleccionado");
        } if( !($(this).is(':checked')) ){
            // Hacer algo si el checkbox ha sido deseleccionado
            //alert("El checkbox con valor " + $(this).val() + " ha sido deseleccionado");
            //$('fieldset').removeAttr('disabled');
            $('.ocultar-mostrar').show('slow');

            $('input:text[name="nombre"]').val('');
            $('input:text[name="apPaterno"]').val('');
            $('input:text[name="apMaterno"]').val('');
            $('input:text[name="calle"]').val('');
            $('input:text[name="numInt"]').val('');
            $('input:text[name="numExt"]').val('');
            $('input:text[name="colonia"]').val('');
            $('input:text[name="cp"]').val('');
            $('select[name="id_estado"]').val(0);
            $('select[name="id_municipio"]').val('placeholder');
            $('#telefono').val('');
            $('#email').val('');
        }
    });

  });



});
